package xyz.arthmc.themoneymod.util;


import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.level.storage.DimensionDataStorage;
import net.minecraftforge.server.ServerLifecycleHooks;

public class CurrenciesSaveData extends SavedData {

    public static String[][] currencies = new String[500][8];

    public CurrenciesSaveData() {
        super();
    }

    public static void init() {
        for (int i = 0; i < 500; i++) {
            currencies[i][0] = "undefined";
            currencies[i][1] = "undefined";
            currencies[i][2] = "undefined";
            currencies[i][3] = "undefined";
            currencies[i][4] = "undefined";
            currencies[i][5] = "undefined";
            currencies[i][6] = "undefined";
            currencies[i][7] = "undefined";
        }
    }

    @Override
    public CompoundTag save(CompoundTag compoundTag) {
        System.out.println("saving business data to world");
        //if buisnesses[i][0] is null, init is called
        if (currencies[0][0] == null) {
            init();
        }
        //writes buisnesses to the compoundTag
        for (int i = 0; i < 500; i++) {
            if (currencies[i][0].equals("undefined")) {
                break;
            }
            CompoundTag buisnessTag = new CompoundTag();
            buisnessTag.putString("name", currencies[i][0]);
            buisnessTag.putString("type", currencies[i][1]);
            buisnessTag.putString("owner", currencies[i][2]);
            buisnessTag.putString("amount", currencies[i][3]);
            buisnessTag.putString("initialAmount", currencies[i][4]);

            compoundTag.put(i + "", buisnessTag);
        }
        return compoundTag;
    }


    public static void addCurrency(String name, String type, String owner, int size) {
        if (currencies[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (currencies[i][0].equals("undefined")) {
                currencies[i][0] = name;
                currencies[i][1] = type;
                currencies[i][2] = owner;
                currencies[i][3] = size + "";
                currencies[i][4] = size + "";

                break;
            }
        }
    }

    public static void printMoreCurrency(String name, int amount) {
        if (currencies[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (currencies[i][0].equals(name)) {
                currencies[i][3] =(currencies[i][3]+ amount);
                break;
            }
        }
    }

    public static String[][] getCurrencies() {
        if (currencies[0][0] == null) {
            init();
        }
        //run loadFromWorld without causing the static error
        CurrenciesSaveData data = new CurrenciesSaveData();
        data.loadFromWorld();

        return currencies;
    }

    public String[][] getCurrenciesNonStatic() {

        if (currencies[0][0] == null) {
            init();
        }
        return currencies;
    }

    public CurrenciesSaveData create() {
        return new CurrenciesSaveData();
    }

    public CurrenciesSaveData load(CompoundTag tag) {
        CurrenciesSaveData data = new CurrenciesSaveData();
        for (int i = 0; i < 500; i++) {
            if (tag.get(i + "") == null) {
                break;
            }
            CompoundTag buisnessTag = tag.getCompound(i + "");
            currencies[i][0] = buisnessTag.getString("name");
            currencies[i][1] = buisnessTag.getString("type");
            currencies[i][2] = buisnessTag.getString("owner");
            currencies[i][3] = buisnessTag.getString("amount");
            currencies[i][4] = buisnessTag.getString("initialAmount");

        }
        return data;
    }

    public static Boolean isNameTaken(String name) {
        if (currencies[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (currencies[i][0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void saveToWorld() {
        //if on server side
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            CurrenciesSaveData data = overworldDataStorage.computeIfAbsent(this::load, this::create, "currencies");
            data.setDirty();
        }
    }

    public void loadFromWorld() {
        //if on server side
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            CurrenciesSaveData data = overworldDataStorage.get(this::load, "currencies");

            if (data != null && data.getCurrenciesNonStatic()[0][0] != null) {


                for (int i = 0; i < 500; i++) {
                    if (data.getCurrenciesNonStatic()[i][0] == null) {
                        break;
                    }
                    currencies[i][0] = data.getCurrenciesNonStatic()[i][0];
                    currencies[i][1] = data.getCurrenciesNonStatic()[i][1];
                    currencies[i][2] = data.getCurrenciesNonStatic()[i][2];
                    currencies[i][3] = data.getCurrenciesNonStatic()[i][3];
                    currencies[i][4] = data.getCurrenciesNonStatic()[i][4];
                }
            } else {
                init();
            }

        }
    }
}
