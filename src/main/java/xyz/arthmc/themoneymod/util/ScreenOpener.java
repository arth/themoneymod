package xyz.arthmc.themoneymod.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;


import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import xyz.arthmc.themoneymod.screen.BuisnessCreationMenu;
import xyz.arthmc.themoneymod.screen.CurrencyPrinterMenu;
import xyz.arthmc.themoneymod.screen.StockExchangeMenu;
import xyz.arthmc.themoneymod.screen.StockSetMenu;

public class ScreenOpener {

    private static final Deque<Screen> backStack = new ArrayDeque<>();
    private static Screen backSteppedFrom = null;
    private static String blockPos;

    public static void open(String s) {
        Screen screen = switch (s) {
            case "BuisnessCreationMenu" -> new BuisnessCreationMenu();
            case "CurrencyPrinterMenu" -> new CurrencyPrinterMenu(1);
            case "StockExchangeMenu" -> new StockExchangeMenu(1, blockPos);
            case "StockSetMenu" -> new StockSetMenu(1);
            default -> null;
        };
        if (screen != null)
        open(Minecraft.getInstance().screen, screen);
    }

    public static void open(@Nullable Screen current, Screen toOpen) {
        /*backSteppedFrom = null;
        if (current != null) {
            if (backStack.size() >= 15) // don't go deeper than 15 steps
                backStack.pollLast();

            backStack.push(current);
        } else
            backStack.clear();
        */
        openScreen(toOpen);
    }

    public static void setBlockPos(String s) {
        blockPos = s;
    }




    private static void openScreen(Screen screen) {
        Minecraft.getInstance()
                .tell(() -> {
                    Minecraft.getInstance()
                            .setScreen(screen);

                });
    }

}