package xyz.arthmc.themoneymod.util;


import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.level.storage.DimensionDataStorage;
import net.minecraftforge.server.ServerLifecycleHooks;
import org.apache.logging.log4j.core.jmx.Server;

public class BuisnessesSaveData extends SavedData {

    public static String[][] buisnesses = new String[500][8];

    private static BuisnessesSaveData instance;

    public BuisnessesSaveData() {
        super();
    }

    public static void init() {
        for (int i = 0; i < 500; i++) {
            buisnesses[i][0] = "undefined";
            buisnesses[i][1] = "undefined";
            buisnesses[i][2] = "undefined";
            buisnesses[i][3] = "undefined";
            buisnesses[i][4] = "undefined";
            buisnesses[i][5] = "undefined";
            buisnesses[i][6] = "undefined";
            buisnesses[i][7] = "undefined";
        }
    }

    @Override
    public CompoundTag save(CompoundTag compoundTag) {
        System.out.println("saving business data to world");
        //if buisnesses[i][0] is null, init is called
        if (buisnesses[0][0] == null) {
            init();
        }
        //writes buisnesses to the compoundTag
        for (int i = 0; i < 500; i++) {
            if (buisnesses[i][0].equals("undefined")) {
                break;
            }
            CompoundTag buisnessTag = new CompoundTag();
            buisnessTag.putString("name", buisnesses[i][0]);
            buisnessTag.putString("exchangeRatio", buisnesses[i][1]);
            buisnessTag.putString("exchangeItem", buisnesses[i][2]);
            buisnessTag.putString("owner", buisnesses[i][3]);
            compoundTag.put(i + "", buisnessTag);
        }
        return compoundTag;
    }


    public static void addBuisness(String name, String exchangeRatio, String exchangeItem, String owner) {
        if (buisnesses[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (buisnesses[i][0].equals("undefined")) {
                buisnesses[i][0] = name;
                buisnesses[i][1] = exchangeRatio;
                buisnesses[i][2] = exchangeItem;
                buisnesses[i][3] = owner;

                break;
            }
        }
    }

    public static String[][] getBuisnesses() {
        if (buisnesses[0][0] == null) {
            init();
        }
        //run loadFromWorld without causing the static error
        BuisnessesSaveData data = new BuisnessesSaveData();
        data.loadFromWorld();

        return buisnesses;
    }

    public String[][] getBuisnessesNonStatic() {

        if (buisnesses[0][0] == null) {
            init();
        }
        return buisnesses;
    }

    public static Boolean isNameTaken(String name) {
        if (buisnesses[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (buisnesses[i][0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public BuisnessesSaveData create() {
        return new BuisnessesSaveData();
    }

    public BuisnessesSaveData load(CompoundTag tag) {
        BuisnessesSaveData data = new BuisnessesSaveData();
        for (int i = 0; i < 500; i++) {
            if (tag.get(i + "") == null) {
                break;
            }
            CompoundTag buisnessTag = tag.getCompound(i + "");
            buisnesses[i][0] = buisnessTag.getString("name");
            buisnesses[i][1] = buisnessTag.getString("exchangeRatio");
            buisnesses[i][2] = buisnessTag.getString("exchangeItem");
            buisnesses[i][3] = buisnessTag.getString("owner");
        }
        return data;
    }

    public void saveToWorld() {
        System.out.println("saving business data to world");
        //if on server side
        Level world = ServerLifecycleHooks.getCurrentServer().getLevel(ServerLevel.OVERWORLD);
        if (!world.isClientSide()) {
            System.out.println("saving business data to world2");
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            BuisnessesSaveData data = overworldDataStorage.computeIfAbsent(this::load, this::create, "buisnesses");
            data.setDirty();
        }
    }

    public static void markInstanceDirty() {
        if (instance != null) {
            instance.setDirty();
        }
    }

    public static void setInstance(BuisnessesSaveData data) {
        instance = data;
    }

    public static BuisnessesSaveData getInstance() {
        return instance;
    }

    public void loadFromWorld() {
        //if on server side
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            BuisnessesSaveData data = overworldDataStorage.get(this::load, "buisnesses");

            if (data != null && data.getBuisnessesNonStatic()[0][0] != null) {


                for (int i = 0; i < 500; i++) {
                    if (data.getBuisnessesNonStatic()[i][0] == null) {
                        break;
                    }
                    buisnesses[i][0] = data.getBuisnessesNonStatic()[i][0];
                    buisnesses[i][1] = data.getBuisnessesNonStatic()[i][1];
                    buisnesses[i][2] = data.getBuisnessesNonStatic()[i][2];
                    buisnesses[i][3] = data.getBuisnessesNonStatic()[i][3];
                }
            } else {
                init();
            }
        }

    }
}
