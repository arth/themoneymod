package xyz.arthmc.themoneymod.util;


import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.level.storage.DimensionDataStorage;
import net.minecraftforge.server.ServerLifecycleHooks;

import java.util.Objects;

public class StockExchangesSaveData extends SavedData {

    public static String[][] exchanges = new String[500][1000];

    public StockExchangesSaveData() {
        super();
    }

    public static void init() {
        for (int i = 0; i < 500; i++) {
            for (int j = 0; j < 1000; j++) {
                exchanges[i][j] = "undefined";
            }
        }
        StockExchangesSaveData data = new StockExchangesSaveData();
        data.loadFromWorld();


    }

    @Override
    public CompoundTag save(CompoundTag compoundTag) {
        System.out.println("saving exchange data to world");
        //if exchanges[i][0] is null, init is called
        if (exchanges[0][0] == null) {
            init();
        }
        //writes exchanges to the compoundTag
        for (int i = 0; i < 500; i++) {
            if (exchanges[i][0].equals("undefined")) {
                break;
            }
            CompoundTag exchangeTag = new CompoundTag();
            exchangeTag.putString("blockPos", exchanges[i][0]);
            for (int j = 1; j < 1000; j++) {
                if (exchanges[i][j].equals("undefined")) {
                    break;
                } else if (Objects.equals(exchanges[i][j], "")) {
                    exchangeTag.putString(j + "", "undefined");
                    break;
                } else {

                        exchangeTag.putString(j + "", exchanges[i][j]);

                }
            }
            compoundTag.put(i+"", exchangeTag);
        }
        return compoundTag;
    }

    public StockExchangesSaveData load(CompoundTag tag) {
        System.out.println("loading exchanges");
        StockExchangesSaveData data = new StockExchangesSaveData();
        for (int i = 0; i < 500; i++) {
            if (tag.get(i + "") == null) {
                //break;
            }
            CompoundTag buisnessTag = tag.getCompound(i + "");
            exchanges[i][0] = buisnessTag.getString("blockPos");

            for (int j = 1; j < 1000; j++) {

                if (buisnessTag.getString(j + "").equals("undefined")) {
                    //break;
                } else {
                    exchanges[i][j] = buisnessTag.getString(j + "");
                }
            }
        }
        return data;
    }

    public static String[] addExchange(String name) {
        if (exchanges[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (exchanges[i][0].equals("undefined") || exchanges[i][0].isEmpty() || exchanges[i][0] == null) {
                exchanges[i][0] = name;
                return exchanges[i];

            }
        }
        return null;

    }

    public static void updateExchange(String name, String[] data) {
        if (exchanges[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (exchanges[i][0].equals(name)) {
                for (int j = 1; j < 1000; j++) {
                    exchanges[i][j] = data[j];
                }
                break;
            }
        }
    }



    public static void removeExchange(String name) {
        if (exchanges[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (exchanges[i][0].equals(name)) {
                exchanges[i][0] = "undefined";
                for (int j = 1; j < 1000; j++) {
                    exchanges[i][j] = "undefined";
                }
                break;
            }
        }
    }

    public static String[][] getExchanges() {
        if (exchanges[0][0] == null) {
            init();
        }


        return exchanges;
    }

    public static String[] getExchange(String name) {

        if (exchanges[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
                if (exchanges[i][0].contains(name)) {
                    return exchanges[i];

            }
        }
        return null;
    }

    public String[] getExchangeNonStatic(String name) {

        if (exchanges[0][0] == null) {
            init();
        }
        for (int i = 0; i < 500; i++) {
            if (exchanges[i][0].contains(name)) {

                return exchanges[i];
            }
        }
        return null;
    }

    public String[][] getExchangesNonStatic() {

        if (exchanges[0][0] == null) {
            init();
        }
        return exchanges;
    }

    public StockExchangesSaveData create() {
        return new StockExchangesSaveData();
    }



    public void saveToWorld() {
        //if on server side
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            StockExchangesSaveData data = overworldDataStorage.computeIfAbsent(this::load, this::create, "exchanges");
            data.setDirty();
        }
    }

    public void loadFromWorld() {
        //if on server side
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
            ServerLevel overworld = server.getLevel(ServerLevel.OVERWORLD);
            DimensionDataStorage overworldDataStorage = overworld.getDataStorage();
            StockExchangesSaveData data = overworldDataStorage.get(this::load, "exchanges");

            if (data != null && data.getExchangesNonStatic()[0][0] != null) {
                System.out.println("loading exchanges");


                for (int i = 0; i < 500; i++) {

                    if (data.getExchangesNonStatic()[i][0] == null) {
                        //break;
                    }

                    for (int j = 0; j < 1000; j++) {

                        if (data.getExchangesNonStatic()[i][j] != null) {


                            exchanges[i][j] = data.getExchangesNonStatic()[i][j];

                        }
                    }

                }
            } else {
                //init();
            }

        }
    }
}
