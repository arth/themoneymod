package xyz.arthmc.themoneymod.networking;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;
import xyz.arthmc.themoneymod.TheMoneyMod;
import xyz.arthmc.themoneymod.networking.packet.MakeBusiness;
import xyz.arthmc.themoneymod.networking.packet.PrintMoney;
import xyz.arthmc.themoneymod.networking.packet.SendBusinessesData;

public class ModPackets {


    private static SimpleChannel INSTANCE;
    private static int packetId = 0;
    private static int id() {
        return packetId++;
    }

    public static void register() {
        SimpleChannel net = NetworkRegistry.ChannelBuilder
                .named(new ResourceLocation(TheMoneyMod.MOD_ID, "packets"))
                .networkProtocolVersion(() -> "1.0")
                .clientAcceptedVersions(s -> true)
                .serverAcceptedVersions(s -> true)
                .simpleChannel();
        INSTANCE = net;

        net.messageBuilder(PrintMoney.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(PrintMoney::new)
                .encoder(PrintMoney::toBytes)
                .consumer(PrintMoney::handle)
                .add();

        net.messageBuilder(MakeBusiness.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(MakeBusiness::new)
                .encoder(MakeBusiness::toBytes)
                .consumer(MakeBusiness::handle)
                .add();

        net.messageBuilder(SendBusinessesData.class, id(), NetworkDirection.PLAY_TO_CLIENT)
                .decoder(SendBusinessesData::new)
                .encoder(SendBusinessesData::toBytes)
                .consumer(SendBusinessesData::handle)
                .add();
    }

    public static <MSG> void sendToServer(MSG message) {
        INSTANCE.sendToServer(message);
    }

    public static <MSG> void sendToPlayer(MSG message, ServerPlayer player) {
        INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), message);
    }
}
