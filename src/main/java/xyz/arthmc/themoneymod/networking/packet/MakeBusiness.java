package xyz.arthmc.themoneymod.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.CurrenciesSaveData;

import java.util.function.Supplier;

public class MakeBusiness {
    private final String name;
    private final String exchangeItem;

    private final String exchangeRatio;

    private final String exchangeCurrency;

    public MakeBusiness(String name, String exchangeItem, String exchangeRatio, String exchangeCurrency) {
        this.name = name;
        this.exchangeItem = exchangeItem;
        this.exchangeRatio = exchangeRatio;
        this.exchangeCurrency = exchangeCurrency;


    }

    public MakeBusiness(FriendlyByteBuf buf) {
        //read the message
        name = buf.readUtf();
        exchangeItem = buf.readUtf();
        exchangeRatio = buf.readUtf();
        exchangeCurrency = buf.readUtf();


    }



    public void toBytes(FriendlyByteBuf buf) {

        buf.writeUtf(name);
        buf.writeUtf(exchangeItem);
        buf.writeUtf(exchangeRatio);
        buf.writeUtf(exchangeCurrency);
    }

    public boolean handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            ServerPlayer player = context.getSender();
            // Serverside
            String itemString;
            if (CurrenciesSaveData.isNameTaken(exchangeCurrency) && exchangeItem.equals("themoneymod:money")) {
                itemString = "themoneymod:money:" + exchangeCurrency;
            } else {
                itemString = exchangeItem;
            }

            BuisnessesSaveData.addBuisness(name, exchangeRatio, itemString, player.getUUID().toString());
            //this calls the saveToWorld method without causing the static error
            BuisnessesSaveData data = new BuisnessesSaveData();
            data.saveToWorld();
            ModPackets.sendToPlayer(new SendBusinessesData(BuisnessesSaveData.getInstance()), player);

            // take 64 pieces of paper from the player and give them a share certificate
            int count = 0;
            for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
                // Get the item stack in the current slot
                ItemStack stack = player.getInventory().getItem(i);

                // if stocksCollected is less than stocks*exchangeRatio and stack is greater
                // or equal to stocks*exchangeRatio, take only stocks*exchangeRatio
                // if the stack is smaller, than take it all and add it to the counter
                if (count < 64 && stack.getItem().toString().equals("paper")) {
                    if (stack.getCount() >= 64 - count) {
                        stack.shrink(64 - count);
                        count = 64;
                    } else if (stack.getItem().toString().equals("paper")) {
                        count += stack.getCount();
                        stack.shrink(stack.getCount());
                    }
                }
            }
            //give 256 SHARE_CERTIFICATE with added nbt "businessName":"businessName" to the player.
            ItemStack shareCertificate = new ItemStack(ModItems.SHARE_CERTIFICATE.get(), 256);
            shareCertificate.getOrCreateTag().putString("businessName", name);

            player.getInventory().add(shareCertificate);
        });
        return true;
    }


}
