package xyz.arthmc.themoneymod.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import xyz.arthmc.themoneymod.item.ModItems;

import java.util.function.Supplier;

public class PrintMoney {
    private final int amount;
    private final String currencyName;

    private final String currencyType;

    public PrintMoney(int amount, String currencyName, String currencyType) {
        this.amount = amount;
        this.currencyName = currencyName;
        this.currencyType = currencyType;

    }

    public PrintMoney(FriendlyByteBuf buf) {
        //read the message
        amount = buf.readInt();
        currencyName = buf.readUtf();
        currencyType = buf.readUtf();


    }



    public void toBytes(FriendlyByteBuf buf) {

        buf.writeInt(amount);
        buf.writeUtf(currencyName);
        buf.writeUtf(currencyType);
    }

    public boolean handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            // Serverside
            ServerPlayer player = context.getSender();
            ServerLevel world = player.getLevel();

            ItemStack money = new ItemStack(ModItems.MONEY.get(), amount);
            money.getOrCreateTag().putString("currencyName", currencyName);
            money.getOrCreateTag().putString("currencyType", currencyType);
            player.getInventory().add(money);
            takePaper(amount, player);
        });
        return true;
    }

    private void takePaper(int amount, ServerPlayer player) {
        // take 64 pieces of paper from the player and give them a share certificate
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            // Get the item stack in the current slot
            ItemStack stack = player.getInventory().getItem(i);

            // if stocksCollected is less than stocks*exchangeRatio and stack is greater
            // or equal to stocks*exchangeRatio, take only stocks*exchangeRatio
            // if the stack is smaller, than take it all and add it to the counter
            if (count < (int)Math.ceil((double)amount /4) && stack.getItem().toString().equals("paper")) {
                if (stack.getCount() >= amount/4 - count) {
                    stack.shrink(amount/4 - count);
                    count = (int)Math.ceil((double)amount /4);
                } else if (stack.getItem().toString().equals("paper")) {
                    count += stack.getCount();
                    stack.shrink(stack.getCount());
                }
            }
        }
    }
}
