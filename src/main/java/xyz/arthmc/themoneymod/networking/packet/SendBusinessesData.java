package xyz.arthmc.themoneymod.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;

import java.util.function.Supplier;

public class SendBusinessesData {

    public BuisnessesSaveData buisnesses;
    public SendBusinessesData(BuisnessesSaveData buisnesses) {
        this.buisnesses = buisnesses;



    }

    public SendBusinessesData(FriendlyByteBuf buf) {





    }



    public void toBytes(FriendlyByteBuf buf) {

    }

    public boolean handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
           BuisnessesSaveData.setInstance(buisnesses);
        });
        return true;
    }


}
