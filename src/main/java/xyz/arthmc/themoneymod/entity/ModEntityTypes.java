package xyz.arthmc.themoneymod.entity;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import xyz.arthmc.themoneymod.TheMoneyMod;
import xyz.arthmc.themoneymod.entity.custom.FinancierEntity;

public class ModEntityTypes {
    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES =
            DeferredRegister.create(ForgeRegistries.ENTITIES, TheMoneyMod.MOD_ID);

    public static final RegistryObject<EntityType<FinancierEntity>> FINANCIER =
            ENTITY_TYPES.register("financier",
                    () -> EntityType.Builder.of(FinancierEntity::new, MobCategory.CREATURE)
                            .sized(0.8f, 0.6f)
                            .build(new ResourceLocation(TheMoneyMod.MOD_ID, "financier").toString()));


    public static void register(IEventBus eventBus) {
        ENTITY_TYPES.register(eventBus);
    }
}