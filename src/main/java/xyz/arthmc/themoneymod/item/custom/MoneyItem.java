package xyz.arthmc.themoneymod.item.custom;

import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class MoneyItem extends Item {
    public MoneyItem(Properties pProperties) {
        super(pProperties);
        //add the nbt property "businessName" to the item's lore

        ItemStack itemStack = new ItemStack(this);




    }


    //overrides appendHoverText to add the business name to the item's lore.
    @Override
    public void appendHoverText(ItemStack pStack, Level pLevel, java.util.List<net.minecraft.network.chat.Component> pTooltip, net.minecraft.world.item.TooltipFlag pFlag) {
        super.appendHoverText(pStack, pLevel, pTooltip, pFlag);
        if (!pStack.getOrCreateTag().getString("currencyName").isEmpty()) {

            pTooltip.add(new TextComponent("§7" + pStack.getOrCreateTag().getString("currencyName")));



        }
        if (!pStack.getOrCreateTag().getString("currencyType").isEmpty()) {
            String s = "Type: Unknown";
            if (pStack.getOrCreateTag().getString("currencyType").equals("no_printing"))
                s = "Type: No Printing";
            else if (pStack.getOrCreateTag().getString("currencyType").equals("limited_printing"))
                s = "Type: Limited Printing";
            else if (pStack.getOrCreateTag().getString("currencyType").equals("unlimited_printing"))
                s = "Type: Unlimited Printing";
            pTooltip.add(new TextComponent("§8" + s));


        }

    }



}
