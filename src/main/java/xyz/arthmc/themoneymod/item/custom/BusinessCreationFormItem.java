package xyz.arthmc.themoneymod.item.custom;

import net.minecraft.client.Minecraft;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import xyz.arthmc.themoneymod.screen.BuisnessCreationMenu;
import xyz.arthmc.themoneymod.screen.StockExchangeSetupMenu;
import xyz.arthmc.themoneymod.util.ScreenOpener;

public class BusinessCreationFormItem extends Item {
    public BusinessCreationFormItem(Properties pProperties) {
        super(pProperties);


    }

    public InteractionResultHolder<ItemStack> use(Level pLevel, Player pPlayer, InteractionHand pHand) {
        if (pLevel.isClientSide) {
            ScreenOpener.open("BuisnessCreationMenu");
        }
        return InteractionResultHolder.sidedSuccess(pPlayer.getItemInHand(pHand), pLevel.isClientSide());
    }


}
