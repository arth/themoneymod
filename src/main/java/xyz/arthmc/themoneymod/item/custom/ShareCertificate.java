package xyz.arthmc.themoneymod.item.custom;

import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ShareCertificate extends Item {
    public ShareCertificate(Properties pProperties) {
        super(pProperties);
        //add the nbt property "businessName" to the item's lore

        ItemStack itemStack = new ItemStack(this);
        String businessName = itemStack.getOrCreateTag().getString("businessName");


    }


    //overrides appendHoverText to add the business name to the item's lore.
    @Override
    public void appendHoverText(ItemStack pStack, Level pLevel, java.util.List<net.minecraft.network.chat.Component> pTooltip, net.minecraft.world.item.TooltipFlag pFlag) {
        super.appendHoverText(pStack, pLevel, pTooltip, pFlag);
        if (!pStack.getOrCreateTag().getString("businessName").isEmpty()) {

            pTooltip.add(new TextComponent("§7" + pStack.getOrCreateTag().getString("businessName")));
        }

    }



}
