package xyz.arthmc.themoneymod.item;

import net.minecraft.world.food.FoodProperties;

public class ModFoods {
    public static final FoodProperties MAPLE_SAP = (new FoodProperties.Builder())
            .nutrition(1)
            .saturationMod(0.1F)
            .build();

    public static final FoodProperties MAPLE_SYRUP = (new FoodProperties.Builder())
            .nutrition(2)
            .saturationMod(0.2F)
            .build();

    public static final FoodProperties PLAIN_PANCAKES = (new FoodProperties.Builder())
            .nutrition(4)
            .saturationMod(0.3F)
            .build();

    public static final FoodProperties SWEET_PANCAKES = (new FoodProperties.Builder())
            .nutrition(6)
            .saturationMod(0.5F)
            .build();

    public static final FoodProperties CLASSIC_PANCAKES = (new FoodProperties.Builder())
            .nutrition(5)
            .saturationMod(0.5F)
            .build();
}
