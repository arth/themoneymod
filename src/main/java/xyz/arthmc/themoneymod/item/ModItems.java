package xyz.arthmc.themoneymod.item;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import xyz.arthmc.themoneymod.TheMoneyMod;
import xyz.arthmc.themoneymod.item.custom.BusinessCreationFormItem;
import xyz.arthmc.themoneymod.item.custom.MoneyItem;
import xyz.arthmc.themoneymod.item.custom.ShareCertificate;

public class ModItems {
    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, TheMoneyMod.MOD_ID);


    public static final RegistryObject<Item> SHARE_CERTIFICATE = ITEMS.register("share_certificate",() ->
            new ShareCertificate(new Item.Properties().fireResistant().stacksTo(64)));

    public static final RegistryObject<Item> MONEY = ITEMS.register("money", () ->
            new MoneyItem(new Item.Properties().stacksTo(64)));

    public static final RegistryObject<Item> FINANCIAL_PROCESSING_UNIT = ITEMS.register("financial_processing_unit", () ->
            new Item(new Item.Properties().tab(CreativeModeTab.TAB_TOOLS).stacksTo(64)));






    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
