package xyz.arthmc.themoneymod;

import com.mojang.logging.LogUtils;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.MinecraftForge;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.ModBlocks;
import xyz.arthmc.themoneymod.block.entity.ModBlockEntities;
import xyz.arthmc.themoneymod.entity.ModEntityTypes;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.sound.ModSounds;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(TheMoneyMod.MOD_ID)
public class TheMoneyMod
{
    // Directly reference a slf4j logger
    private static final Logger LOGGER = LogUtils.getLogger();
    public static final String MOD_ID = "themoneymod";

    public TheMoneyMod() {


        // Register the enqueueIMC method for modloading
        IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();
        ModItems.register(eventBus);
        ModSounds.register(eventBus);
        ModBlocks.register(eventBus);
        ModBlockEntities.register(eventBus);
        ModEntityTypes.register(eventBus);

        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);

    }
    private void clientSetup(final FMLCommonSetupEvent event) {
        ItemBlockRenderTypes.setRenderLayer(ModBlocks.RADIO_STOCK_SET.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(ModBlocks.DIGITAL_STOCK_SET.get(), RenderType.cutout());


    }
    private void setup(final FMLCommonSetupEvent event)
    {
        // some preinit code
        LOGGER.info("HELLO FROM PREINIT");
        LOGGER.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
        ModPackets.register();
    }



}
