package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.ForgeRegistries;
import org.checkerframework.checker.units.qual.C;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.entity.custom.StockExchangeBlockEntity;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;

public class StockExchangeSetupMenu extends Screen {

    private EditBox nameField; // the text field for the buisness name

    private EditBox exchangeItemField; // the text field for the exchange item
    private Button confirmButton; // the button to confirm the name
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu

    private Boolean nameValid = false;
    private Boolean itemValid = true;

    private StockExchangeBlockEntity stockExchangeBlockEntity;

    public StockExchangeSetupMenu(StockExchangeBlockEntity stockExchangeBlockEntity2) {
        super(new TextComponent("Setup Stock Exchange"));
        stockExchangeBlockEntity = stockExchangeBlockEntity2; // the title of the menu
    }

    @Override
    protected void init() {
        // create the text field with a default text
        nameField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -10, WIDTH - 20, 20, new TextComponent("Business Name"));
        nameField.setMaxLength(32); // set the maximum length of the name
        nameField.setValue(""); // clear the default text
        nameField.setResponder(this::onNameChanged); // set the responder to handle name changes
        addWidget(nameField); // add the text field to the menu

        // create the text field with a default text
        exchangeItemField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30, WIDTH - 20, 20, new TextComponent("Exchange Item"));
        exchangeItemField.setMaxLength(64);
        exchangeItemField.setValue("minecraft:diamond");
        exchangeItemField.setResponder(this::onExchangeItemChanged);
        addWidget(exchangeItemField);

        // create the confirm button with a label
        confirmButton = new Button(width / 2 - WIDTH / 2 + 10, height / 2 + HEIGHT / 2 - 30, WIDTH - 20, 20, new TextComponent("Create"), this::onConfirmPressed);


        confirmButton.active = false; // disable the button by default
        addWidget(confirmButton); // add the button to the menu
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT, 0xFFFFFF);

        // render the fields and their titles
        drawString(poseStack, font, "Exchange Name", width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -10 - 12, 0xFFFFFF);
        nameField.render(poseStack, mouseX, mouseY, partialTicks);

        //drawString(poseStack, font, "Exchange Item", width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30 - 12, 0xFFFFFF);
       // exchangeItemField.render(poseStack, mouseX, mouseY, partialTicks);

        confirmButton.render(poseStack, mouseX, mouseY, partialTicks);

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    // a method to handle name changes
    private void onNameChanged(String name) {
        // check if the name is valid
        nameValid = name != null && !name.isEmpty() && name.matches("[A-Za-z0-9 ]+");

        // enable or disable the button accordingly
        confirmButton.active = nameValid;
    }

    private void onExchangeItemChanged(String name) {
        // check if the item exists in the game
        Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
        itemValid = !item.toString().equals("air");


        // enable or disable the button accordingly
        confirmButton.active = nameValid && itemValid;
    }

    // a method to handle confirm button press
    public void onConfirmPressed(Button button) {
        // get the name from the text field
        String name = nameField.getValue();
        stockExchangeBlockEntity.setup = true;
        stockExchangeBlockEntity.exchangeName = name;
        minecraft.setScreen(new StockExchangeMenu(1, stockExchangeBlockEntity.getBlockPos().toString()));
    }




}





