package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Player;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.entity.custom.StockExchangeBlockEntity;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.ScreenOpener;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import java.util.Objects;

public class StockExchangeMenu extends Screen {
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 300; // the width of the menu
    private static final int HEIGHT = 200; // the height of the menu
    private static final int MARGIN = 10; //             if (businesses[0][0] != "undefined") {the margin between the elements
    private static final int ROW_HEIGHT = 20; // the height of each row
    private static final int COLUMNS = 2; // the number of columns to display
    private static final int COLUMN_WIDTH = WIDTH / (COLUMNS + 2); // Adjusted for 2 new columns
    private static final String[] COLUMN_NAMES = {new TranslatableComponent("menus.themoneymod.name").getString(), new TranslatableComponent("menus.themoneymod.price").getString(), "Buy", "Sell"}; // Added "Buy" and "Sell" columns

    private String[][] businesses;

    private Button closeButton;


    
    private Button manageButton;

    private Button createButton;


    private Button indexPlusButton;
    private Button indexMinusButton;

    private String blockPos;



    public StockExchangeMenu(int index2, String position) {
        super(new TranslatableComponent("menus.themoneymod.stock_exchange"));
        index = index2;
        businesses = BuisnessesSaveData.getBuisnesses();
        this.blockPos = position;
    }
    private Button buyButton0;
    private Button sellButton0;

    private Button buyButton1;
    private Button sellButton1;
    private Button buyButton2;
    private Button sellButton2;
    private Button buyButton3;
    private Button sellButton3;
    private Button buyButton4;
    private Button sellButton4;
    private Button buyButton5;
    private Button sellButton5;

    private Button buyButton6;
    private Button sellButton6;
    private Button buyButton7;
    private Button sellButton7;

    PoseStack poseStack2;
    int mouseX2;
    int mouseY2;
    float partialTicks2;

    private int index = 1;
    @Override
    protected void init() {
        indexMinusButton = new Button(width / 2 - WIDTH / 2 + MARGIN, height*7 / 8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.last_page").getString()+(index-1)), this::onIndexMinusPressed);
        indexPlusButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH, height*7 / 8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.next_page").getString()+(index+1)), this::onIndexPlusPressed);

        closeButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*3, height*8 / 10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.done"), this::onClosePressed);

        manageButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*2, height*8 / 10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.manage"), this::onManagePressed);
        createButton = new Button(width / 2 - WIDTH / 2, height*8 / 10, WIDTH/3 + MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.create_business"), this::onCreatePressed);
        addWidget(indexPlusButton);
        addWidget(indexMinusButton);



        buyButton0 = new Button(getListButtonX(1), getListButtonY(1), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed0);
        sellButton0 = new Button(getListButtonX(2), getListButtonY(1), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed0);
        buyButton1 = new Button(getListButtonX(1), getListButtonY(2), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed1);
        sellButton1 = new Button(getListButtonX(2), getListButtonY(2), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed1);
        buyButton2 = new Button(getListButtonX(1), getListButtonY(3), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed2);
        sellButton2 = new Button(getListButtonX(2), getListButtonY(3), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed2);
        buyButton3 = new Button(getListButtonX(1), getListButtonY(4), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed3);
        sellButton3 = new Button(getListButtonX(2), getListButtonY(4), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed3);
        buyButton4 = new Button(getListButtonX(1), getListButtonY(5), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed4);
        sellButton4 = new Button(getListButtonX(2), getListButtonY(5), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed4);
        buyButton5 = new Button(getListButtonX(1), getListButtonY(6), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed5);
        sellButton5 = new Button(getListButtonX(2), getListButtonY(6), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed5);
        buyButton6 = new Button(getListButtonX(1), getListButtonY(7), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed6);
        sellButton6 = new Button(getListButtonX(2), getListButtonY(7), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed6);
        buyButton7 = new Button(getListButtonX(1), getListButtonY(8), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.buy"), this::onBuyPressed7);
        sellButton7 = new Button(getListButtonX(2), getListButtonY(8), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.sell"), this::onSellPressed7);








        if (!Objects.equals(businesses[((index*8)-8)][0], "undefined")) {
            addWidget(buyButton0);
            addWidget(sellButton0);
        }
        if (!Objects.equals(businesses[1+((index*8)-8)][0], "undefined")) {
            addWidget(buyButton1);
            addWidget(sellButton1);
        }
        if (!Objects.equals(businesses[2+((index*8)-8)][0], "undefined")) {
            addWidget(buyButton2);
            addWidget(sellButton2);
        }
        if (!Objects.equals(businesses[3+((index*8)-8)][0], "undefined")) {
        addWidget(buyButton3);
        addWidget(sellButton3);
        }

        if (!Objects.equals(businesses[4+((index*8)-8)][0], "undefined")) {
        addWidget(buyButton4);
        addWidget(sellButton4);
        }
        if (!Objects.equals(businesses[5+((index*8)-8)][0], "undefined")) {
        addWidget(buyButton5);
        addWidget(sellButton5);
        }
        if (!Objects.equals(businesses[6+((index*8)-8)][0], "undefined")) {
        addWidget(buyButton6);
        addWidget(sellButton6);
        }
        if (!Objects.equals(businesses[7+((index*8)-8)][0], "undefined")) {
        addWidget(buyButton7);
        addWidget(sellButton7);
        }




        addWidget(closeButton);
        addWidget(manageButton);
        addWidget(createButton);
    }

    private int getListButtonX(int col) {
        return (int) ( width / 2 - WIDTH / 2 + MARGIN + (2+(.675*(col-1))) * COLUMN_WIDTH) + 30;
    }

    private int getListButtonY(int row) {
        return height / 2 - HEIGHT / 2 + MARGIN + (ROW_HEIGHT * row)-6;
    }



    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        poseStack2 = poseStack;
        mouseX2 = mouseX;


        mouseY2 = mouseY;
        partialTicks2 = partialTicks;
        renderBackground(poseStack);

        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT / 2 - MARGIN, 0xFFFFFF);



                drawString(poseStack, font, COLUMN_NAMES[0], width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);

            drawString(poseStack, font, COLUMN_NAMES[1], (width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH)+30, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);





        for (int i = 0; i < businesses.length && i < 8; i++) {
            if (businesses[i][0].equals("undefined")) {
                break;
            }
            int rowY = height / 2 - HEIGHT / 2 + MARGIN + ROW_HEIGHT * (i + 1);
            String[] business = businesses[i+((index*8)-8)];

            String itemString;
            try {
                if (business[2].contains("themoneymod:money")) {
                    itemString = "§a"+business[2].split(":")[2];
                } else {
                    itemString = new TranslatableComponent("item." + business[2].split(":")[0] + "." + business[2].split(":")[1]).getString();
                }
            } catch (Exception e) {
                itemString = business[2];
            }
            if (!Objects.equals(businesses[i+((index*8)-8)][0], "undefined")) {
                drawString(poseStack, font, business[0], width / 2 - WIDTH / 2, rowY, 0xFFFFFF);
                drawString(poseStack, font, business[1] + "x " + itemString, (width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH)+30, rowY, 0xFFFFFF);
            }



        }

        if (!Objects.equals(businesses[0+((index*8)-8)][0], "undefined")) {
            buyButton0.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton0.active = hasStockInExchange(businesses[0+((index*8)-8)][0]);
            sellButton0.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton0.active = hasExchangeItemsInExchange(businesses[0+((index*8)-8)][0]);

        }
        if (!Objects.equals(businesses[1+((index*8)-8)][0], "undefined")) {
            buyButton1.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton1.active = hasStockInExchange(businesses[1+((index*8)-8)][0]);
            sellButton1.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton1.active = hasExchangeItemsInExchange(businesses[1+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[2+((index*8)-8)][0], "undefined")) {
            buyButton2.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton2.active = hasStockInExchange(businesses[2+((index*8)-8)][0]);
            sellButton2.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton2.active = hasExchangeItemsInExchange(businesses[2+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[3+((index*8)-8)][0], "undefined")) {
            buyButton3.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton3.active = hasStockInExchange(businesses[3+((index*8)-8)][0]);
            sellButton3.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton3.active = hasExchangeItemsInExchange(businesses[3+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[4+((index*8)-8)][0], "undefined")) {
            buyButton4.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton4.active = hasStockInExchange(businesses[4+((index*8)-8)][0]);
            sellButton4.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton4.active = hasExchangeItemsInExchange(businesses[4+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[5+((index*8)-8)][0], "undefined")) {
            buyButton5.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton5.active = hasStockInExchange(businesses[5+((index*8)-8)][0]);
            sellButton5.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton5.active = hasExchangeItemsInExchange(businesses[5+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[6+((index*8)-8)][0], "undefined")) {
            buyButton6.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton6.active = hasStockInExchange(businesses[6+((index*8)-8)][0]);
            sellButton6.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton6.active = hasExchangeItemsInExchange(businesses[6+((index*8)-8)][0]);
        }
        if (!Objects.equals(businesses[7+((index*8)-8)][0], "undefined")) {
            buyButton7.render(poseStack, mouseX, mouseY, partialTicks);
            buyButton7.active = hasStockInExchange(businesses[7+((index*8)-8)][0]);
            sellButton7.render(poseStack, mouseX, mouseY, partialTicks);
            sellButton7.active = hasExchangeItemsInExchange(businesses[7+((index*8)-8)][0]);
        }
        closeButton.render(poseStack, mouseX, mouseY, partialTicks);
        manageButton.render(poseStack, mouseX, mouseY, partialTicks);
        createButton.render(poseStack, mouseX, mouseY, partialTicks);
        if (businesses.length> index*8&& !Objects.equals(businesses[((index+1) * 8) - 8][0], "undefined")) {
            indexPlusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {
            indexMinusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    private void onBuyPressed0(Button button) {
        System.out.println("Buy button pressed");
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed0(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed0(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onSellPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockSellMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onManagePressed(Button button) {
        Minecraft.getInstance().setScreen(new StockExchangeManageMenu(index, blockPos));
    }

    // a method to handle close button press
    private void onClosePressed(Button button) {
        // close the menu
        minecraft.setScreen(null);
    }

    private void onCreatePressed(Button button) {
        Minecraft.getInstance().setScreen(new BuisnessCreationMenu());
    }

    private void onIndexPlusPressed(Button button) {
        if (!Objects.equals(businesses[((index+1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockExchangeMenu(index + 1,blockPos));
        }

    }

    private void onIndexMinusPressed(Button button) {
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockExchangeMenu(index - 1, blockPos));
        }

    }

    private Boolean hasStockInExchange(String businessName) {
        String[] exchange = StockExchangesSaveData.getExchange(blockPos);
        if (exchange != null) {
        for (int i = 1; i < 1000; i++) {
            if (exchange[i].contains("stock;"+businessName)) {
                return (Integer.parseInt(exchange[i].split(";")[2]) > 0);

            }
        }
    }
        return false;
    }

    private Boolean hasExchangeItemsInExchange(String businessName) {
        StockExchangesSaveData data = new StockExchangesSaveData();
        String[] exchange = data.getExchangeNonStatic(blockPos);
        if (exchange != null) {

            for (int i = 1; i < 1000; i++) {
                if (exchange[i].contains("item;"+businessName)) {
                    return Integer.parseInt(exchange[i].split(";")[2]) > 0;
                }
            }
        }
        return false;
    }


}
