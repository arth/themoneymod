package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.networking.packet.PrintMoney;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.CurrenciesSaveData;

import static java.lang.Integer.parseInt;

public class CreateCurrencyUnlimitedPrintingMenu extends Screen {

    private EditBox nameField; // the text field for the buisness name

    private EditBox initialAmountField; // the text field for the exchange item


    private Button confirmButton; // the button to confirm the name
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu

    private Boolean nameValid = false;

    private Boolean amountValid = true;

    private int initialAmount = 512;

    private Player player = Minecraft.getInstance().player;

    public CreateCurrencyUnlimitedPrintingMenu() {
        super(new TranslatableComponent("menus.themoneymod.create_currency")); // the title of the menu
    }

    @Override
    protected void init() {
        // create the text field with a default text
        nameField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -10, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.currency_name"));
        nameField.setMaxLength(16); // set the maximum length of the name
        nameField.setValue(""); // clear the default text
        nameField.setResponder(this::onNameChanged); // set the responder to handle name changes
        addWidget(nameField); // add the text field to the menu

        initialAmountField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.initial_amount_to_print"));
        initialAmountField.setMaxLength(64);
        initialAmountField.setValue("512");
        initialAmountField.setResponder(this::onInitialAmountChanged);
        addWidget(initialAmountField);





        // create the confirm button with a label
        confirmButton = new Button(width / 2 - WIDTH / 2 + 10, height *5/6, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.create_business"), this::onConfirmPressed);


        confirmButton.active = false; // disable the button by default
        addWidget(confirmButton); // add the button to the menu
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT, 0xFFFFFF);

        // render the fields and their titles
        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.currency_name").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -10 - 12, 0xFFFFFF);
        nameField.render(poseStack, mouseX, mouseY, partialTicks);

        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.initial_amount_to_print").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30 - 12, 0xFFFFFF);
        initialAmountField.render(poseStack, mouseX, mouseY, partialTicks);






        drawString(poseStack, font, piecesOfPaper() + "/"+initialAmount/4+" " + new TranslatableComponent("menus.themoneymod.pieces_of_paper").getString(), width / 2 -WIDTH/2+10, height / 2 - HEIGHT / 2 +110 - 12, 0xFFFFFF);

        confirmButton.render(poseStack, mouseX, mouseY, partialTicks);

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    // a method to handle name changes
    private void onNameChanged(String name) {
        // check if the name is valid
        nameValid = name != null && !name.isEmpty() && name.matches("[A-Za-z0-9 :]+");

        // enable or disable the button accordingly
        confirmButton.active = amountValid && nameValid && piecesOfPaper()>=initialAmount/4 && hasSpaceForMoney();
    }

    private void onInitialAmountChanged(String name) {
        // check if the item exists in the game
        if (name.matches("[0-9]+")) {
            amountValid = true;
        }
        try {
            initialAmount = parseInt(name);
        } catch (NumberFormatException e) {
            amountValid = false;
        }

        confirmButton.active = amountValid && nameValid && piecesOfPaper()>=initialAmount/4 && hasSpaceForMoney();
    }

    // a method to handle confirm button press
    public void onConfirmPressed(Button button) {
        // get the name from the text field
        String name = nameField.getValue();
        int amount = parseInt(initialAmountField.getValue());


            ModPackets.sendToServer(new PrintMoney(amount, name, "unlimited_printing"));


        CurrenciesSaveData data = new CurrenciesSaveData();
        data.loadFromWorld();
        CurrenciesSaveData.addCurrency(name, "unlimited_printing", player.getUUID().toString(), initialAmount);
        data.saveToWorld();

        // close the menu
        minecraft.setScreen(null);
    }


    private int piecesOfPaper(){

        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);

            if (!stack.isEmpty() && stack.getItem().toString().equals("paper")) {
                // Add the stack size to the counter
                count += stack.getCount();
            }
        }


        return count;
    }

    private Boolean hasSpaceForMoney(){
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack.isEmpty()) {
                count = count + 64;
            }
        }
        return count >= initialAmount;
    }

}





