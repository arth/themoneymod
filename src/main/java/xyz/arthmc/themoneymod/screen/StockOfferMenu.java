package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;
import org.slf4j.Logger;
import org.w3c.dom.Text;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import java.util.Objects;

import static java.lang.Integer.parseInt;

public class StockOfferMenu extends Screen {

    private Button doneButton; // the button to confirm the name
    private Button plusButton; // the button to increase the number of exchangeItems
    private Button minusButton; // the button to decrease the number of exchangeItems
    private Button collectButton; // the button to collect the stocks

    private Button offerButton;
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu
    private static final int MARGIN = 10; // the margin between the elements
    private static final int CLOSE_BUTTON_SIZE = 20; // the size of the close button
    private static final int PLUS_MINUS_BUTTON_SIZE = 20; // the size of the plus and minus buttons
    private static final int COLLECT_BUTTON_SIZE = 50; // the size of the collect button
    private static final int TEXT_HEIGHT = 10; // the height of the text
    private String businessName;
    private int exchangeRatio; // the ratio of stocks per diamond
    private String exchangeItem;
    private String exchangeItemText;
    private int exchangeItems; // the number of exchangeItems the user has
    private int stocks; // the number of stocks the user can buy

    private int exchangeItemsThatExchangeHas = 0;

    private int index;

    private Player player = Minecraft.getInstance().player;

    private String blockPos;



    public StockOfferMenu(String[] business, int index2, String pos) {


        super(new TextComponent(business[0] + new TranslatableComponent("menus.themoneymod.offer_stock").getString())); // the title of the menu
        this.businessName = business[0];
        this.exchangeRatio = parseInt(business[1]);
        this.exchangeItem = business[2];
        this.index = index2;

        //get translated name of the exchangeItem
        String itemString;
        try {
            if (business[2].contains("themoneymod:money")) {
                itemString = "§a"+business[2].split(":")[2];
            } else {
                itemString = new TranslatableComponent("item." + business[2].split(":")[0] + "." + business[2].split(":")[1]).getString();
            }
        } catch (Exception e) {
            itemString = business[2];
        }
        this.exchangeItemText = itemString;
        this.blockPos = pos;


        stocks = 1;
        exchangeItems = stocks*exchangeRatio;

        String[] exchange = StockExchangesSaveData.getExchange(blockPos);
        if (exchange != null) {
            for (int j = 1; j < 1000; j++) {
                if (exchange[j] != null && exchange[j].contains("item;" + businessName)) {
                    exchangeItemsThatExchangeHas = parseInt(exchange[j].split(";")[2]);
                    break;
                }
            }
        }
    }

    @Override
    protected void init() {

        // create the confirm button with a label
        doneButton = new Button(width / 2 - WIDTH / 2 + MARGIN, height / 2 + HEIGHT / 2 - MARGIN + 10, WIDTH - 2 * MARGIN, 20, new TranslatableComponent("menus.themoneymod.back"), this::onConfirmPressed);

        // create the plus button with a label
        plusButton = new Button(width / 2 - WIDTH / 4 - MARGIN - PLUS_MINUS_BUTTON_SIZE, height / 2 - 20 / 2, PLUS_MINUS_BUTTON_SIZE, 20, new TranslatableComponent("menus.themoneymod.plus"), this::onPlusPressed);

        // create the minus button with a label
        minusButton = new Button(width / 2 - WIDTH / 4 + MARGIN, height / 2 - 20 / 2, PLUS_MINUS_BUTTON_SIZE, 20, new TranslatableComponent("menus.themoneymod.minus"), this::onMinusPressed);

        // create the collect button with a label
        collectButton = new Button(width / 2 + WIDTH / 4- COLLECT_BUTTON_SIZE/2, height / 2 + 20, COLLECT_BUTTON_SIZE, 20, new TranslatableComponent("menus.themoneymod.collect"), this::onCollectPressed);

        offerButton = new Button(width / 2 - WIDTH / 4 - COLLECT_BUTTON_SIZE/2, height / 2 + 20, COLLECT_BUTTON_SIZE, 20, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed);

        addWidget(doneButton); // add the confirm button to the menu
        addWidget(plusButton); // add the plus button to the menu
        addWidget(minusButton); // add the minus button to the menu
        addWidget(collectButton); // add the collect button to the menu
        addWidget(offerButton); // add the collect button to the menu
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT / 2, 0xFFFFFF);

        // render the buttons
        doneButton.render(poseStack, mouseX, mouseY, partialTicks);
        plusButton.render(poseStack, mouseX, mouseY, partialTicks);
        minusButton.render(poseStack, mouseX, mouseY, partialTicks);
        collectButton.render(poseStack, mouseX, mouseY, partialTicks);
        collectButton.active = hasSpaceForExchangeItems() && exchangeItemsThatExchangeHas>0; // enable the collect button
        offerButton.render(poseStack, mouseX, mouseY, partialTicks);
        offerButton.active = hasEnoughShares();

        // render the text for the exchangeItems and the stocks
        drawCenteredString(poseStack, font, stocks + "x " + new TranslatableComponent("menus.themoneymod.shares").getString(), width / 2 - WIDTH / 4, height / 2 - TEXT_HEIGHT - COLLECT_BUTTON_SIZE / 2, 0xFFFFFF);
        String collectText = exchangeItemsThatExchangeHas + "x " + exchangeItemText;
        if (exchangeItemsThatExchangeHas==0) collectText = "0x " + exchangeItemText + " earned, check back later";
        drawCenteredString(poseStack, font, new TranslatableComponent("menus.themoneymod.offer1").getString(), width / 2 + WIDTH / 4, height / 2 - TEXT_HEIGHT - COLLECT_BUTTON_SIZE / 2 + 20, 0x808080);
        drawCenteredString(poseStack, font, new TranslatableComponent("menus.themoneymod.offer2").getString(), width / 2 + WIDTH / 4, height / 2 - TEXT_HEIGHT - COLLECT_BUTTON_SIZE / 2 +30, 0x808080);
        drawCenteredString(poseStack, font, new TranslatableComponent("menus.themoneymod.offer3").getString(), width / 2 + WIDTH / 4, height / 2 - TEXT_HEIGHT - COLLECT_BUTTON_SIZE / 2 + 40, 0x808080);
        drawCenteredString(poseStack, font, exchangeItemsThatExchangeHas + "x " + exchangeItemText, width / 2 + WIDTH / 4, height / 2 - TEXT_HEIGHT - COLLECT_BUTTON_SIZE / 2, 0xFFFFFF);


        //drawCenteredString(poseStack, font, "You can earn money by buying shares from this", width / 2, height / 2 + HEIGHT / 2 - 30, 0xFFFFFF);
        //drawCenteredString(poseStack, font, " exchange after people have bought them", width / 2, height / 2 + HEIGHT / 2 - 20, 0xFFFFFF);
        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    // a method to handle confirm button press
    public void onConfirmPressed(Button button){Minecraft.getInstance().setScreen(new StockExchangeManageMenu(index, blockPos));
    }

    // a method to handle plus button press
    public void onPlusPressed(Button button) {
        stocks++;
        exchangeItems = exchangeItems + exchangeRatio;
        offerButton.active = hasEnoughShares(); // enable the collect button
    }

    // a method to handle minus button press
    public void onMinusPressed(Button button) {
        if (exchangeItems > 0) { // check if the exchangeItems are positive
            stocks--;
            exchangeItems = exchangeItems - exchangeRatio;
            if (stocks == 0) {
                offerButton.active = false; // disable the collect button
            }
        }
    }

    // a method to handle collect button press
    public void onOfferPressed(Button button) {

        if (hasEnoughShares()) {

            int stocksCollected = 0;
            for (int i = 0; i < player.getInventory().getContainerSize(); i++) {

                ItemStack stack = player.getInventory().getItem(i);

                Boolean isShareCertificate = stack.getItem().toString().equals("share_certificate");
                Boolean isBusiness = stack.getOrCreateTag().getString("businessName").equals(businessName);
                if (stocksCollected < stocks && isShareCertificate && isBusiness) {
                    if (stack.getCount() >= stocks - stocksCollected) {
                        stack.shrink(stocks - stocksCollected);
                        stocksCollected = stocks;
                    } else {
                        stocksCollected += stack.getCount();
                        stack.shrink(stack.getCount());
                    }
                }


            }


            String[] exchange = StockExchangesSaveData.getExchange(blockPos);
            if (exchange == null) {




                exchange = StockExchangesSaveData.addExchange(blockPos);
                assert exchange != null;
                exchange[0] = blockPos;
            }
            Boolean foundStock = false;

            //if the stock is already in the exchange, this increases the amount

            if (exchange.length > 0) {
                for (int j = 1; j < 1000; j++) {

                    if (exchange[j] != null && exchange[j].contains("stock;" + businessName)) {
                        System.out.println("found stock" + exchange[j]);
                        exchange[j] = "stock;" + businessName + ";" + (parseInt(exchange[j].split(";")[2]) + stocksCollected);
                        System.out.println("modified stock" + exchange[j]);
                        foundStock = true;
                        StockExchangesSaveData.updateExchange(blockPos, exchange);
                        break;
                    }
                }
            }

            //if the stock is not in the exchange, this adds it
            if (!foundStock) {
                int index = 1;
                for (int j = 1; j < 1000; j++) {
                    if (Objects.equals(exchange[j], "undefined") | exchange[j] == null | exchange[j].equals("")) {
                        index = j;
                        break;
                    }
                }
                exchange[index] = "stock;" + businessName + ";" + stocksCollected;
                StockExchangesSaveData.updateExchange(blockPos, exchange);


            }


            StockExchangesSaveData data = new StockExchangesSaveData();
            data.saveToWorld();
            //make the button temporarily inactive
            offerButton.active = false;
            //after 1 second, make the button active again
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            offerButton.active = hasEnoughShares();
                        }
                    },
                    1000
            );

            //this is to provide feedback to the user that the offer was successful
        }
    }

    public void onCollectPressed(Button button) {
        if (hasSpaceForExchangeItems()) {
            ItemStack money = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(exchangeItem)), exchangeItemsThatExchangeHas);
            player.getInventory().add(money);
        }
        String[] exchange = StockExchangesSaveData.getExchange(blockPos);
if (exchange != null) {
            for (int j = 1; j < 1000; j++) {
                if (exchange[j] != null && exchange[j].contains("item;" + businessName)) {
                    exchange[j] = "item;" + businessName + ";" + 0;
                    StockExchangesSaveData.updateExchange(blockPos, exchange);
                    break;
                }
            }
        }
exchangeItemsThatExchangeHas = 0;
    collectButton.active = false; // disable the collect button


    }



    public Boolean hasEnoughShares(){

        // Initialize a counter for the shares
        int count = 0;
        // Loop over the inventory slots
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            // Get the item stack in the current slot
            ItemStack stack = player.getInventory().getItem(i);
            Boolean isShareCertificate = stack.getItem().toString().equals("share_certificate");
            Boolean isBusiness = stack.getOrCreateTag().getString("businessName").equals(businessName);
            // Check if the item stack is not empty and matches the exchange item
            if (!stack.isEmpty() && isShareCertificate && isBusiness) {
                // Add the stack size to the counter
                count += stack.getCount();
            }
        }

        // Compare the counter with the exchange items
        return stocks != 0 && count >= stocks;
    }

    private Boolean hasSpaceForExchangeItems(){
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack.isEmpty()) {
                count+=64;
            }
        }
        return count >= exchangeItemsThatExchangeHas;
    }
}
