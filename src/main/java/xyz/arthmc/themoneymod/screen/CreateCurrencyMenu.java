package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import org.slf4j.Logger;

public class CreateCurrencyMenu extends Screen {


    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu

    private static int H_PADDING = 60; // the horizontal padding

    private Button noPrintingButton;

    private Button limitedPrintingButton;

    private Button unlimitedPrintingButton;

    private Button closeButton;

    private int index;

    public CreateCurrencyMenu(int index2) {
        super(new TranslatableComponent("menus.themoneymod.create_currency")); // the title of the menu
        index = index2;
    }

    @Override
    protected void init() {
        H_PADDING = width/4;
        noPrintingButton = new Button(H_PADDING-30, height*4/8+25, 60, 20, new TranslatableComponent("menus.themoneymod.select"), this::onNoPrintingButtonClicked);
        addRenderableWidget(noPrintingButton);

        limitedPrintingButton = new Button(width/2-30, height*4/8+25, 60, 20, new TranslatableComponent("menus.themoneymod.select"), this::onLimitedPrintingButtonClicked);
        addRenderableWidget(limitedPrintingButton);

        closeButton = new Button(width / 2 - (height*5/8+20)/2, height*8/10, height*5/8+20, 20, new TranslatableComponent("menus.themoneymod.back"), this::onClosePressed);
        addRenderableWidget(closeButton);
        unlimitedPrintingButton = new Button(width-H_PADDING-30, height*4/8+25, 60, 20, new TranslatableComponent("menus.themoneymod.select"), this::onUnlimitedPrintingButtonClicked);
        addRenderableWidget(unlimitedPrintingButton);
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);

        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting"), H_PADDING, height*2/8, 0xFFFFFF);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting"), width/2, height*2/8, 0xFFFFFF);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.unlimitedprinting"), width-H_PADDING, height*2/8, 0xFFFFFF);

        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting1"), H_PADDING, height*3/8, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting2"), H_PADDING, height*3/8+10, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting3"), H_PADDING, height*3/8+20, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting4"), H_PADDING, height*3/8+30, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.noprinting5"), H_PADDING, height*3/8+40, 0x808080);

        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting1"), width/2, height*3/8, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting2"), width/2, height*3/8+10, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting3"), width/2, height*3/8+20, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting4"), width/2, height*3/8+30, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.limitedprinting5"), width/2, height*3/8+40, 0x808080);

        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.unlimitedprinting1"), width-H_PADDING, height*3/8, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.unlimitedprinting2"), width-H_PADDING, height*3/8+10, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.unlimitedprinting3"), width-H_PADDING, height*3/8+20, 0x808080);
        drawCenteredString(poseStack, this.font, new TranslatableComponent("menus.themoneymod.unlimitedprinting4"), width-H_PADDING, height*3/8+30, 0x808080);


        //drawCenteredString(poseStack, this.font, new TranslatableComponent("You can print as much of your currency as you want in the future."), width-H_PADDING, height*3/8, 16711680);

        noPrintingButton.render(poseStack, mouseX, mouseY, partialTicks);
        limitedPrintingButton.render(poseStack, mouseX, mouseY, partialTicks);
        unlimitedPrintingButton.render(poseStack, mouseX, mouseY, partialTicks);

        closeButton.render(poseStack, mouseX, mouseY, partialTicks);


        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    private void onNoPrintingButtonClicked(Button button) {
        minecraft.setScreen(new CreateCurrencyNoPrintingMenu());
    }

    private void onLimitedPrintingButtonClicked(Button button) {
        minecraft.setScreen(new CreateCurrencyLimitedPrintingMenu());
    }

    private void onUnlimitedPrintingButtonClicked(Button button) {
        minecraft.setScreen(new CreateCurrencyUnlimitedPrintingMenu());
    }

    private void onClosePressed(Button button) {
        minecraft.setScreen(new CurrencyPrinterMenu(index));
    }




}





