package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.networking.packet.PrintMoney;
import xyz.arthmc.themoneymod.util.CurrenciesSaveData;

import java.util.Objects;

import static java.lang.Integer.parseInt;

public class CurrencyPrintMenu extends Screen {

    private EditBox nameField; // the text field for the buisness name

    private EditBox printingAmountField; // the text field for the exchange item


    private Button confirmButton; // the button to confirm the name
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu

    private Boolean nameValid = false;

    private Boolean amountValid = true;

    private int printingAmount = 512;

    private Player player = Minecraft.getInstance().player;

    private String[] currency;
    private int index;
    private String blockPos;

    private int max = -1;

    public CurrencyPrintMenu(String[] currency2, int index2, String blockPos2) {

        super(new TextComponent(new TranslatableComponent("menus.themoneymod.print_more").getString() +currency2[0])); // the title of the menu
        this.currency = currency2;
        this.index = index2;
        this.blockPos = blockPos2;
    }

    @Override
    protected void init() {


        printingAmountField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.initial_amount_to_print"));
        printingAmountField.setMaxLength(64);
        printingAmountField.setValue("512");
        printingAmountField.setResponder(this::onInitialAmountChanged);
        addWidget(printingAmountField);





        // create the confirm button with a label
        confirmButton = new Button(width / 2 - WIDTH / 2 + 10, height *5/6, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.create_business"), this::onConfirmPressed);


        confirmButton.active = false; // disable the button by default
        addWidget(confirmButton); // add the button to the menu
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT, 0xFFFFFF);

        try {
            max = parseInt(currency[4]) * 3 - parseInt(currency[3]);
        } catch (NumberFormatException e) {

        }
        String type = currency[1];
        int initialSize = parseInt(currency[4]);
        int currentSize = parseInt(currency[3]);
        String s = new TranslatableComponent("menus.themoneymod.amount_to_print").getString()+" (Max "+max+")";
        if ((Objects.equals(type, "limited_printing")) && (max < 0)) max = 0;
        else if (Objects.equals(type, "unlimited_printing")) s = new TranslatableComponent("menus.themoneymod.amount_to_print").getString();



        drawString(poseStack, font, s, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +30 - 12, 0xFFFFFF);
        printingAmountField.render(poseStack, mouseX, mouseY, partialTicks);








        drawString(poseStack, font, piecesOfPaper() + "/"+ (int)Math.ceil((double)printingAmount /4)+" "+ new TranslatableComponent("menus.themoneymod.pieces_of_paper").getString(), width / 2 -WIDTH/2+10, height / 2 - HEIGHT / 2 +110 - 12, 0xFFFFFF);

        confirmButton.render(poseStack, mouseX, mouseY, partialTicks);

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    private void takePaper(int amount) {
        // take 64 pieces of paper from the player and give them a share certificate
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            // Get the item stack in the current slot
            ItemStack stack = player.getInventory().getItem(i);

            // if stocksCollected is less than stocks*exchangeRatio and stack is greater
            // or equal to stocks*exchangeRatio, take only stocks*exchangeRatio
            // if the stack is smaller, than take it all and add it to the counter
            if (count < (int)Math.ceil((double)amount /4) && stack.getItem().toString().equals("paper")) {
                if (stack.getCount() >= amount/4 - count) {
                    stack.shrink(amount/4 - count);
                    count = (int)Math.ceil((double)amount /4);
                } else if (stack.getItem().toString().equals("paper")) {
                    count += stack.getCount();
                    stack.shrink(stack.getCount());
                }
            }
        }
    }

    private void onInitialAmountChanged(String name) {
        int initialSize = parseInt(currency[4]);
        int currentSize = parseInt(currency[3]);
        // check if the item exists in the game
        if (name.matches("[0-9]+")) {
            amountValid = true;
        }
        try {
            printingAmount = parseInt(name);
        } catch (NumberFormatException e) {
            amountValid = false;
        }

        confirmButton.active = amountValid && piecesOfPaper()>= (int)Math.ceil((double)printingAmount /4)  && hasSpaceForMoney() && parseInt(name)+currentSize <= initialSize*3;
    }

    // a method to handle confirm button press
    public void onConfirmPressed(Button button) {
        int amount = parseInt(printingAmountField.getValue());
        String name = currency[0];
        String type = currency[1];
        int initialSize = parseInt(currency[4]);
        int currentSize = parseInt(currency[3]);

        System.out.println("12678 type: "+type + " amount: "+amount + " currentSize: "+currentSize + " initialSize: "+initialSize);
        Boolean canPrint = true;
        if ((Objects.equals(type, "limited_printing")) && (amount + currentSize == initialSize*3)) {

            ModPackets.sendToServer(new PrintMoney(amount, name, "limited_printing"));
        } else if ((Objects.equals(type, "limited_printing")) && (amount + currentSize < initialSize*3)) {
            ModPackets.sendToServer(new PrintMoney(amount, name, "limited_printing"));


        }else if (Objects.equals(type, "unlimited_printing")) {
            ModPackets.sendToServer(new PrintMoney(amount, name, "unlimited_printing"));
        } else {
            canPrint = false;
        }


        if (canPrint) {
            CurrenciesSaveData data = new CurrenciesSaveData();
            data.loadFromWorld();
            CurrenciesSaveData.printMoreCurrency(name, amount);
            data.saveToWorld();
        }


        // close the menu
        minecraft.setScreen(null);
    }


    private int piecesOfPaper(){

        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);

            if (!stack.isEmpty() && stack.getItem().toString().equals("paper")) {
                // Add the stack size to the counter
                count += stack.getCount();
            }
        }


        return count;
    }

    private Boolean hasSpaceForMoney(){
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack.isEmpty()) {
                count = count + 64;
            }
        }
        return count >= printingAmount;
    }

}





