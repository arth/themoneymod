package xyz.arthmc.themoneymod.screen;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.ScreenOpener;

import java.util.Objects;

public class StockSetMenu extends Screen {
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 300; // the width of the menu
    private static final int HEIGHT = 200; // the height of the menu
    private static final int MARGIN = 10; // the margin between the elements
    private static final int ROW_HEIGHT = 20; // the height of each row
    private static final int COLUMNS = 2; // the number of columns to display
    private static final int COLUMN_WIDTH = WIDTH / (COLUMNS + 2); // the width of each column
    private static final String[] COLUMN_NAMES = {new TranslatableComponent("menus.themoneymod.name").getString(), new TranslatableComponent("menus.themoneymod.price").getString()}; // the names of the columns

    private int index  = 1;

    private Button closeButton;

    private Button createButton;

    private Button indexPlusButton;
    private Button indexMinusButton;

    public String[][] businesses;

    public StockSetMenu(int index2) {

        super(new TranslatableComponent("menus.themoneymod.stock_set")); // the title of the menu
        index = index2;


    }

    @Override
    protected void init() {
        indexMinusButton = new Button(width / 2 - WIDTH / 2 + MARGIN, height*7/8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.last_page").toString() + (index-1)), this::onIndexMinusPressed);
        indexPlusButton = new Button(width / 2 - WIDTH / 2 + MARGIN + WIDTH/5, height*7/8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.next_page").toString() + (index+1)), this::onIndexPlusPressed);
        closeButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*3, height*8/10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.back"), this::onClosePressed);
        createButton = new Button(width / 2 - WIDTH / 2, height*8 / 10, WIDTH/3 + MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.create_business"), this::onCreatePressed);
        addWidget(createButton);
        addWidget(closeButton);
        addWidget(indexPlusButton);
        addWidget(indexMinusButton);
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT / 2 - MARGIN, 0xFFFFFF);


        // render the column names
        for (int i = 0; i < COLUMNS; i++) {
            drawString(poseStack, font, COLUMN_NAMES[i], width / 2 - WIDTH / 2 + MARGIN + i * COLUMN_WIDTH, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);
        }
        businesses = BuisnessesSaveData.getBuisnesses();
        // render the stocks
        for (int i = 0; i < businesses.length && i < 8; i++) {

            if (businesses[i+(index*8)-8][0].equals("undefined")) {
                break;
            }
            String[] business = businesses[i+(index*8)-8];
            int rowY = height / 2 - HEIGHT / 2 + MARGIN + ROW_HEIGHT * (i + 1); // calculate the y position of the row
            String itemString;
            try {
                if (business[2].contains("themoneymod:money")) {
                    itemString = "§a"+business[2].split(":")[2];
                } else {
                    itemString = new TranslatableComponent("item." + business[2].split(":")[0] + "." + business[2].split(":")[1]).getString();
                }
            } catch (Exception e) {
                itemString = business[2];
            }

            // render the name
            drawString(poseStack, font, business[0], width / 2 - WIDTH / 2 + MARGIN, rowY, 0xFFFFFF);

            // render the price
            drawString(poseStack, font, business[1] + "x " + itemString, width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH, rowY, 0xFFFFFF);
            // render the item
            //minecraft.getItemRenderer().renderGuiItem(new ItemStack((ItemLike) new ResourceLocation(buisness[2])), width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH * 2, rowY); // draw the item icon
            //blit(poseStack, width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH * 2, rowY, 0, 0, 16, 16, 16, 16); // draw the item icon
        }

        closeButton.render(poseStack, mouseX, mouseY, partialTicks);
        createButton.render(poseStack, mouseX, mouseY, partialTicks);
        if (businesses.length> index*8 && !Objects.equals(businesses[((index+1) * 8) -8 ][0], "undefined")) {
            indexPlusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {
            indexMinusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    // a method to handle close button press
    private void onClosePressed(Button button) {
        // close the menu
        minecraft.setScreen(null);
    }

    private void onCreatePressed(Button button) {
        ScreenOpener.open("BuisnessCreationMenu");
    }

    private void onIndexPlusPressed(Button button) {
        if (!Objects.equals(businesses[((index+1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockSetMenu(index + 1));
        }

    }

    private void onIndexMinusPressed(Button button) {
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockSetMenu(index - 1));
        }

    }
}
