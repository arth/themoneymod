package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Player;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.entity.custom.StockExchangeBlockEntity;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.ScreenOpener;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import java.util.Objects;

public class StockExchangeManageMenu extends Screen {
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 300; // the width of the menu
    private static final int HEIGHT = 200; // the height of the menu
    private static final int MARGIN = 10; //             if (businesses[0][0] != "undefined") {the margin between the elements
    private static final int ROW_HEIGHT = 20; // the height of each row
    private static final int COLUMNS = 2; // the number of columns to display
    private static final int COLUMN_WIDTH = WIDTH / (COLUMNS + 2); // Adjusted for 2 new columns
    private static final String[] COLUMN_NAMES = {new TranslatableComponent("menus.themoneymod.name").getString(), new TranslatableComponent("menus.themoneymod.price").getString(), "Buy", "Sell"}; // Added "Buy" and "Sell" columns

    private String[][] businesses;

    private Button closeButton;

    private Button manageButton;

    private Button createButton;


    private Button indexPlusButton;
    private Button indexMinusButton;

    private String blockPos;



    public StockExchangeManageMenu(int index2, String position) {
        super(new TranslatableComponent("menus.themoneymod.stock_exchange"));
        index = index2;
        businesses = BuisnessesSaveData.getBuisnesses();
        this.blockPos = position;
    }
    private Button offerButton0;
    private Button withdrawButton0;

    private Button offerButton1;
    private Button withdrawButton1;
    private Button offerButton2;
    private Button withdrawButton2;
    private Button offerButton3;
    private Button withdrawButton3;
    private Button offerButton4;
    private Button withdrawButton4;
    private Button offerButton5;
    private Button withdrawButton5;

    private Button offerButton6;
    private Button withdrawButton6;
    private Button offerButton7;
    private Button withdrawButton7;

    PoseStack poseStack2;
    int mouseX2;
    int mouseY2;
    float partialTicks2;

    private int index = 1;
    @Override
    protected void init() {
        indexMinusButton = new Button(width / 2 - WIDTH / 2 + MARGIN, height*7 / 8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.last_page").getString()+(index-1)), this::onIndexMinusPressed);
        indexPlusButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH, height*7 / 8, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.next_page").getString()+(index+1)), this::onIndexPlusPressed);

        closeButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*3, height*8 / 10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.done"), this::onClosePressed);

        manageButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*2, height*8 / 10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.trade"), this::onManagePressed);
        createButton = new Button(width / 2 - WIDTH / 2, height*8 / 10, WIDTH/3 + MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.create_business"), this::onCreatePressed);

        addWidget(indexPlusButton);
        addWidget(indexMinusButton);
        addWidget(manageButton);
        addWidget(createButton);

        offerButton0 = new Button(getListButtonX(1), getListButtonY(1), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed0);
        withdrawButton0 = new Button(getListButtonX(2), getListButtonY(1), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed0);
        offerButton1 = new Button(getListButtonX(1), getListButtonY(2), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed1);
        withdrawButton1 = new Button(getListButtonX(2), getListButtonY(2), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed1);
        offerButton2 = new Button(getListButtonX(1), getListButtonY(3), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed2);
        withdrawButton2 = new Button(getListButtonX(2), getListButtonY(3), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed2);
        offerButton3 = new Button(getListButtonX(1), getListButtonY(4), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed3);
        withdrawButton3 = new Button(getListButtonX(2), getListButtonY(4), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed3);
        offerButton4 = new Button(getListButtonX(1), getListButtonY(5), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed4);
        withdrawButton4 = new Button(getListButtonX(2), getListButtonY(5), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed4);
        offerButton5 = new Button(getListButtonX(1), getListButtonY(6), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed5);
        withdrawButton5 = new Button(getListButtonX(2), getListButtonY(6), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed5);
        offerButton6 = new Button(getListButtonX(1), getListButtonY(7), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed6);
        withdrawButton6 = new Button(getListButtonX(2), getListButtonY(7), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed6);
        offerButton7 = new Button(getListButtonX(1), getListButtonY(8), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.offer"), this::onOfferPressed7);
        withdrawButton7 = new Button(getListButtonX(2), getListButtonY(8), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.withdraw"), this::onWithdrawPressed7);








        if (!Objects.equals(businesses[((index*8)-8)][0], "undefined")) {
            addWidget(offerButton0);
            addWidget(withdrawButton0);
        }
        if (!Objects.equals(businesses[1+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton1);
            addWidget(withdrawButton1);
        }
        if (!Objects.equals(businesses[2+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton2);
            addWidget(withdrawButton2);
        }
        if (!Objects.equals(businesses[3+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton3);
            addWidget(withdrawButton3);
        }

        if (!Objects.equals(businesses[4+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton4);
            addWidget(withdrawButton4);
        }
        if (!Objects.equals(businesses[5+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton5);
            addWidget(withdrawButton5);
        }
        if (!Objects.equals(businesses[6+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton6);
            addWidget(withdrawButton6);
        }
        if (!Objects.equals(businesses[7+((index*8)-8)][0], "undefined")) {
            addWidget(offerButton7);
            addWidget(withdrawButton7);
        }




        addWidget(closeButton);
    }

    private int getListButtonX(int col) {
        return (int) ( width / 2 - WIDTH / 2 + MARGIN + (2+(.675*(col-1))) * COLUMN_WIDTH+30);
    }

    private int getListButtonY(int row) {
        return height / 2 - HEIGHT / 2 + MARGIN + (ROW_HEIGHT * row)-6;
    }



    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        poseStack2 = poseStack;
        mouseX2 = mouseX;
        mouseY2 = mouseY;
        partialTicks2 = partialTicks;
        renderBackground(poseStack);

        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT / 2 - MARGIN, 0xFFFFFF);


                //this is to give a bit of extra space for the first column, as it contains business names
                drawString(poseStack, font, COLUMN_NAMES[0], width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);

                drawString(poseStack, font, COLUMN_NAMES[1], (width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH)+30, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);





        for (int i = 0; i < businesses.length && i < 8; i++) {
            if (businesses[i][0].equals("undefined")) {
                break;
            }
            int rowY = height / 2 - HEIGHT / 2 + MARGIN + ROW_HEIGHT * (i + 1);
            String[] business = businesses[i+((index*8)-8)];

            String itemString;
            try {
                if (business[2].contains("themoneymod:money")) {
                    itemString = "§a"+business[2].split(":")[2];
                } else {
                    itemString = new TranslatableComponent("item." + business[2].split(":")[0] + "." + business[2].split(":")[1]).getString();
                }
            } catch (Exception e) {
                itemString = business[2];
            }
            if (!Objects.equals(businesses[i+((index*8)-8)][0], "undefined")) {
                drawString(poseStack, font, business[0], width / 2 - WIDTH / 2, rowY, 0xFFFFFF);
                drawString(poseStack, font, business[1] + "x " + itemString, (width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH)+30, rowY, 0xFFFFFF);
            }



        }
        Player player = Minecraft.getInstance().player;
        if (!Objects.equals(businesses[0+((index*8)-8)][0], "undefined")) {
            offerButton0.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton0.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton0.active = Objects.equals(Objects.requireNonNull(businesses[0+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton0.active = Objects.equals(Objects.requireNonNull(businesses[0+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[1+((index*8)-8)][0], "undefined")) {
            offerButton1.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton1.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton1.active = Objects.equals(Objects.requireNonNull(businesses[1+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton1.active = Objects.equals(Objects.requireNonNull(businesses[1+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[2+((index*8)-8)][0], "undefined")) {
            offerButton2.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton2.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton2.active = Objects.equals(Objects.requireNonNull(businesses[2+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton2.active = Objects.equals(Objects.requireNonNull(businesses[2+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[3+((index*8)-8)][0], "undefined")) {
            offerButton3.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton3.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton3.active = Objects.equals(Objects.requireNonNull(businesses[3+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton3.active = Objects.equals(Objects.requireNonNull(businesses[3+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[4+((index*8)-8)][0], "undefined")) {
            offerButton4.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton4.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton4.active = Objects.equals(Objects.requireNonNull(businesses[4+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton4.active = Objects.equals(Objects.requireNonNull(businesses[4+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[5+((index*8)-8)][0], "undefined")) {
            offerButton5.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton5.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton5.active = Objects.equals(Objects.requireNonNull(businesses[5+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton5.active = Objects.equals(Objects.requireNonNull(businesses[5+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[6+((index*8)-8)][0], "undefined")) {
            offerButton6.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton6.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton6.active = Objects.equals(Objects.requireNonNull(businesses[6+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton6.active = Objects.equals(Objects.requireNonNull(businesses[6+((index*8)-8)][3]), player.getUUID().toString());
        }
        if (!Objects.equals(businesses[7+((index*8)-8)][0], "undefined")) {
            offerButton7.render(poseStack, mouseX, mouseY, partialTicks);
            withdrawButton7.render(poseStack, mouseX, mouseY, partialTicks);
            offerButton7.active = Objects.equals(Objects.requireNonNull(businesses[7+((index*8)-8)][3]), player.getUUID().toString());
            withdrawButton7.active = Objects.equals(Objects.requireNonNull(businesses[7+((index*8)-8)][3]), player.getUUID().toString());
        }
        closeButton.render(poseStack, mouseX, mouseY, partialTicks);
        manageButton.render(poseStack, mouseX, mouseY, partialTicks);
        createButton.render(poseStack, mouseX, mouseY, partialTicks);
        if (businesses.length> index*8&& !Objects.equals(businesses[((index+1) * 8) - 8][0], "undefined")) {
            indexPlusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {
            indexMinusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    private void onBuyPressed0(Button button) {
        System.out.println("Buy button pressed");
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed0(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed0(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[0+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[1+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[2+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[3+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[4+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[5+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[6+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onOfferPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockOfferMenu(businesses[7+((index*8)-8)],index, blockPos));
    }

    private void onManagePressed(Button button) {
        Minecraft.getInstance().setScreen(new StockExchangeMenu(index, blockPos));
    }

    private void onCreatePressed(Button button) {
        Minecraft.getInstance().setScreen(new BuisnessCreationMenu());
    }

    // a method to handle close button press
    private void onClosePressed(Button button) {
        // close the menu
        minecraft.setScreen(null);
    }

    private void onIndexPlusPressed(Button button) {
        if (!Objects.equals(businesses[((index+1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockExchangeManageMenu(index + 1,blockPos));
        }

    }

    private void onIndexMinusPressed(Button button) {
        if (index-1 > 0 && !Objects.equals(businesses[((index-1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new StockExchangeManageMenu(index - 1, blockPos));
        }

    }

    private Boolean hasStockInExchange(String businessName) {
        String[] exchange = StockExchangesSaveData.getExchange(blockPos);
        if (exchange != null) {
            for (int i = 1; i < 1000; i++) {
                if (exchange[i].contains("stock;"+businessName)) {
                    return (Integer.parseInt(exchange[i].split(";")[2]) > 0);

                }
            }
        }
        return false;
    }

    private Boolean hasExchangeItemsInExchange(String businessName) {
        StockExchangesSaveData data = new StockExchangesSaveData();
        String[] exchange = data.getExchangeNonStatic(blockPos);
        if (exchange != null) {

            for (int i = 1; i < 1000; i++) {
                if (exchange[i].contains("item;"+businessName)) {
                    return Integer.parseInt(exchange[i].split(";")[2]) > 0;
                }
            }
        }
        return false;
    }


}
