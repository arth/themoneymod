package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Player;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.util.CurrenciesSaveData;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import java.util.Objects;

import static java.lang.Integer.parseInt;

public class CurrencyPrinterMenu extends Screen {
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 300; // the width of the menu
    private static final int HEIGHT = 200; // the height of the menu
    private static final int MARGIN = 10; //             if (currencies[0][0] != "undefined") {the margin between the elements
    private static final int ROW_HEIGHT = 20; // the height of each row
    private static final int COLUMNS = 2; // the number of columns to display
    private static final int COLUMN_WIDTH = WIDTH / (COLUMNS + 2); // Adjusted for 2 new columns
    private static final String[] COLUMN_NAMES = {new TranslatableComponent("menus.themoneymod.name").getString(), new TranslatableComponent("menus.themoneymod.type").getString(), "Buy", "Sell"}; // Added "Buy" and "Sell" columns

    private String[][] currencies;

    private Button closeButton;

    private Button createButton;


    private Button indexPlusButton;
    private Button indexMinusButton;

    private String blockPos;



    public CurrencyPrinterMenu(int index2) {
        super(new TranslatableComponent("menus.themoneymod.currency_printer"));
        index = index2;
        currencies = CurrenciesSaveData.getCurrencies();
    }
    private Button printButton0;
    private Button withdrawButton0;

    private Button printButton1;
    private Button withdrawButton1;
    private Button printButton2;
    private Button withdrawButton2;
    private Button printButton3;
    private Button withdrawButton3;
    private Button printButton4;
    private Button withdrawButton4;
    private Button printButton5;
    private Button withdrawButton5;

    private Button printButton6;
    private Button withdrawButton6;
    private Button printButton7;
    private Button withdrawButton7;

    PoseStack poseStack2;
    int mouseX2;
    int mouseY2;
    float partialTicks2;

    private int index = 1;
    @Override
    protected void init() {
        indexMinusButton = new Button(width / 2 - WIDTH / 2 + MARGIN, height*8/10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.last_page").getString()+(index-1)), this::onIndexMinusPressed);
        indexPlusButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH, height*8/10, WIDTH/4 - MARGIN, ROW_HEIGHT, new TextComponent(new TranslatableComponent("menus.themoneymod.next_page").getString()+(index+1)), this::onIndexPlusPressed);

        closeButton = new Button(width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH*3, height*9/12, WIDTH/4 - MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.done"), this::onClosePressed);
        //button to the left of close, says "Create New Currency"
        createButton = new Button(width / 2 - WIDTH / 2, height*9 / 12, WIDTH/3+MARGIN, ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.create_new_currency"), this::onManagePressed);
        addWidget(indexPlusButton);
        addWidget(indexMinusButton);


        printButton0 = new Button(getListButtonX(1), getListButtonY(1), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed0);
        printButton1 = new Button(getListButtonX(1), getListButtonY(2), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed1);
        printButton2 = new Button(getListButtonX(1), getListButtonY(3), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed2);
        printButton3 = new Button(getListButtonX(1), getListButtonY(4), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed3);
        printButton4 = new Button(getListButtonX(1), getListButtonY(5), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed4);
        printButton5 = new Button(getListButtonX(1), getListButtonY(6), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed5);
        printButton6 = new Button(getListButtonX(1), getListButtonY(7), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed6);
        printButton7 = new Button(getListButtonX(1), getListButtonY(8), (int) (COLUMN_WIDTH/1.5), ROW_HEIGHT, new TranslatableComponent("menus.themoneymod.print"), this::onPrintPressed7);
        







        if (!Objects.equals(currencies[((index*8)-8)][0], "undefined")) 
            addWidget(printButton0);
            
        if (!Objects.equals(currencies[1+((index*8)-8)][0], "undefined")) 
            addWidget(printButton1);
            
        if (!Objects.equals(currencies[2+((index*8)-8)][0], "undefined")) 
            addWidget(printButton2);
            
        if (!Objects.equals(currencies[3+((index*8)-8)][0], "undefined")) 
            addWidget(printButton3);

        if (!Objects.equals(currencies[4+((index*8)-8)][0], "undefined")) 
            addWidget(printButton4);
            
        if (!Objects.equals(currencies[5+((index*8)-8)][0], "undefined")) 
            addWidget(printButton5);
            
        if (!Objects.equals(currencies[6+((index*8)-8)][0], "undefined")) 
            addWidget(printButton6);
            
        if (!Objects.equals(currencies[7+((index*8)-8)][0], "undefined")) 
            addWidget(printButton7);
            




        addWidget(closeButton);
        addWidget(createButton);
    }

    private int getListButtonX(int col) {
        return (int) ( width / 2 - WIDTH / 2 + MARGIN + (2+(.675*(col-1))) * COLUMN_WIDTH) + 25;
    }

    private int getListButtonY(int row) {
        return height / 2 - HEIGHT / 2 + MARGIN + (ROW_HEIGHT * row)-6;
    }



    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        poseStack2 = poseStack;
        mouseX2 = mouseX;
        mouseY2 = mouseY;
        partialTicks2 = partialTicks;
        renderBackground(poseStack);

        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT / 2 - MARGIN, 0xFFFFFF);

        for (int i = 0; i < COLUMNS; i++) {
            if(i == 0){
                //this is to give a bit of extra space for the first column, as it contains business names
                drawString(poseStack, font, COLUMN_NAMES[i], width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);
            } else {
                drawString(poseStack, font, COLUMN_NAMES[i], width / 2 - WIDTH / 2 + MARGIN + i * COLUMN_WIDTH, height / 2 - HEIGHT / 2 + MARGIN, 0xFFFFFF);
            }}




        for (int i = 0; i < currencies.length && i < 8; i++) {
            if (currencies[i][0].equals("undefined")) {
                break;
            }
            int rowY = height / 2 - HEIGHT / 2 + MARGIN + ROW_HEIGHT * (i + 1);
            String[] business = currencies[i+((index*8)-8)];

            String itemString = new TranslatableComponent("item."+business[2].replace(":",".")).getString();
            if (!Objects.equals(currencies[i+((index*8)-8)][0], "undefined")) {
                drawString(poseStack, font, business[0], width / 2 - WIDTH / 2, rowY, 0xFFFFFF);
                String currencyType = new TranslatableComponent("menus.themoneymod."+business[1]).getString();
                drawString(poseStack, font, currencyType, width / 2 - WIDTH / 2 + MARGIN + COLUMN_WIDTH, rowY, 0xFFFFFF);
            }



        }

        Player player = Minecraft.getInstance().player;
        if (!Objects.equals(currencies[0+((index*8)-8)][0], "undefined")) 
            printButton0.render(poseStack, mouseX, mouseY, partialTicks);
            printButton0.active = Objects.equals(currencies[0 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[0 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[1+((index*8)-8)][0], "undefined")) 
            printButton1.render(poseStack, mouseX, mouseY, partialTicks);
            printButton1.active = Objects.equals(currencies[1 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[1 + ((index * 8) - 8)]);

            
        if (!Objects.equals(currencies[2+((index*8)-8)][0], "undefined")) 
            printButton2.render(poseStack, mouseX, mouseY, partialTicks);
            printButton2.active = Objects.equals(currencies[2 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[2 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[3+((index*8)-8)][0], "undefined")) 
            printButton3.render(poseStack, mouseX, mouseY, partialTicks);
            printButton3.active = Objects.equals(currencies[3 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[3 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[4+((index*8)-8)][0], "undefined")) 
            printButton4.render(poseStack, mouseX, mouseY, partialTicks);
            printButton4.active = Objects.equals(currencies[4 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[4 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[5+((index*8)-8)][0], "undefined")) 
            printButton5.render(poseStack, mouseX, mouseY, partialTicks);
            printButton5.active = Objects.equals(currencies[5 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[5 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[6+((index*8)-8)][0], "undefined")) 
            printButton6.render(poseStack, mouseX, mouseY, partialTicks);
            printButton6.active = Objects.equals(currencies[6 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[6 + ((index * 8) - 8)]);
            
        if (!Objects.equals(currencies[7+((index*8)-8)][0], "undefined")) 
            printButton7.render(poseStack, mouseX, mouseY, partialTicks);
            printButton7.active = Objects.equals(currencies[7 + ((index * 8) - 8)][2], player.getUUID().toString())
            && canPrint(currencies[7 + ((index * 8) - 8)]);
            
        closeButton.render(poseStack, mouseX, mouseY, partialTicks);
        createButton.render(poseStack, mouseX, mouseY, partialTicks);
        if (currencies.length> index*8&& !Objects.equals(currencies[((index+1) * 8) - 8][0], "undefined")) {
            indexPlusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }
        if (index-1 > 0 && !Objects.equals(currencies[((index-1) * 8) - 8][0], "undefined")) {
            indexMinusButton.render(poseStack, mouseX, mouseY, partialTicks);
        }

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    private Boolean canPrint(String[] currency) {
        if (Objects.equals(currency[1], "limited_printing")) {
            try {
                return parseInt(currency[3]) < parseInt(currency[4]) * 3;
            } catch (Exception e) {
                return false;
            }
        } else if (Objects.equals(currency[1], "unlimited_printing"))
        return true;
        else return false;
    }
    private void onBuyPressed0(Button button) {
        System.out.println("Buy button pressed");
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[0+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed0(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[0+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed0(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[0+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[1+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed1(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[1+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed1(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[1+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[2+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed2(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[2+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed2(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[2+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[3+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed3(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[3+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed3(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[3+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[4+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed4(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[4+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed4(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[4+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[5+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed5(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[5+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed5(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[5+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[6+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed6(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[6+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed6(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[6+((index*8)-8)],index, blockPos));
    }

    private void onBuyPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockBuyMenu(currencies[7+((index*8)-8)],index, blockPos));
    }

    private void onWithdrawPressed7(Button button) {
        Minecraft.getInstance().setScreen(new StockWithdrawMenu(currencies[7+((index*8)-8)],index, blockPos));
    }

    private void onPrintPressed7(Button button) {
        Minecraft.getInstance().setScreen(new CurrencyPrintMenu(currencies[7+((index*8)-8)],index, blockPos));
    }

    private void onManagePressed(Button button) {
        Minecraft.getInstance().setScreen(new CreateCurrencyMenu(index));
    }

    // a method to handle close button press
    private void onClosePressed(Button button) {
        // close the menu
        minecraft.setScreen(null);
    }

    private void onIndexPlusPressed(Button button) {
        if (!Objects.equals(currencies[((index+1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new CurrencyPrinterMenu(index + 1));
        }

    }

    private void onIndexMinusPressed(Button button) {
        if (index-1 > 0 && !Objects.equals(currencies[((index-1) * 8) - 8][0], "undefined")) {

            Minecraft.getInstance().setScreen(new CurrencyPrinterMenu(index - 1));
        }

    }

    private Boolean hasStockInExchange(String businessName) {
        String[] exchange = StockExchangesSaveData.getExchange(blockPos);
        if (exchange != null) {
            for (int i = 1; i < 1000; i++) {
                if (exchange[i].contains("stock;"+businessName)) {
                    return (parseInt(exchange[i].split(";")[2]) > 0);

                }
            }
        }
        return false;
    }

    private Boolean hasExchangeItemsInExchange(String businessName) {
        StockExchangesSaveData data = new StockExchangesSaveData();
        String[] exchange = data.getExchangeNonStatic(blockPos);
        if (exchange != null) {

            for (int i = 1; i < 1000; i++) {
                if (exchange[i].contains("item;"+businessName)) {
                    return parseInt(exchange[i].split(";")[2]) > 0;
                }
            }
        }
        return false;
    }


}
