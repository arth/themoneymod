package xyz.arthmc.themoneymod.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.networking.ModPackets;
import xyz.arthmc.themoneymod.networking.packet.MakeBusiness;
import xyz.arthmc.themoneymod.networking.packet.PrintMoney;
import xyz.arthmc.themoneymod.util.BuisnessesSaveData;
import xyz.arthmc.themoneymod.util.CurrenciesSaveData;

public class BuisnessCreationMenu extends Screen {

    private EditBox nameField; // the text field for the buisness name

    private EditBox exchangeItemField; // the text field for the exchange item
    private EditBox exchangeCurrencyField;

    private EditBox exchangeRatioField; // the text field for the exchange ratio
    private Button confirmButton; // the button to confirm the name
    private static final Logger LOGGER = LogUtils.getLogger();
    private static final int WIDTH = 200; // the width of the menu
    private static final int HEIGHT = 100; // the height of the menu

    private Boolean nameValid = false;
    private Boolean itemValid = true;

    private Boolean ratioValid = true;

    private Player player = Minecraft.getInstance().player;

    public BuisnessCreationMenu() {
        super(new TranslatableComponent("menus.themoneymod.business_creation")); // the title of the menu
    }

    @Override
    protected void init() {
        // create the text field with a default text
        nameField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -25, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.business_name"));
        nameField.setMaxLength(16); // set the maximum length of the name
        nameField.setValue(""); // clear the default text
        nameField.setResponder(this::onNameChanged); // set the responder to handle name changes
        addWidget(nameField); // add the text field to the menu

        // create the text field with a default text
        exchangeItemField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +15, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.exchange_item"));
        exchangeItemField.setMaxLength(64);

        exchangeItemField.setResponder(this::onExchangeItemChanged);

        if (CurrenciesSaveData.getCurrencies().length > 0) {

            exchangeItemField.setValue("themoneymod:money");
            exchangeCurrencyField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 + 55, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.exchange_currency"));
            exchangeCurrencyField.setMaxLength(64);
            exchangeCurrencyField.setValue(CurrenciesSaveData.getCurrencies()[0][0]);
            exchangeCurrencyField.setResponder(this::onExchangeCurrencyChanged);


        } else {
            exchangeItemField.setValue("minecraft:diamond");
        }

        addWidget(exchangeItemField);
        addWidget(exchangeCurrencyField);
        exchangeRatioField = new EditBox(font, width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +95, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.initial_stock_price"));
        exchangeRatioField.setMaxLength(64);
        exchangeRatioField.setValue("1");
        exchangeRatioField.setResponder(this::onExchangeRatioChanged);
        addWidget(exchangeRatioField);



        // create the confirm button with a label
        confirmButton = new Button(width / 2 - WIDTH / 2 + 10, height *7/8, WIDTH - 20, 20, new TranslatableComponent("menus.themoneymod.create_business"), this::onConfirmPressed);


        confirmButton.active = false; // disable the button by default
        addWidget(confirmButton); // add the button to the menu
    }



    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        // render the background and the title
        renderBackground(poseStack);
        drawCenteredString(poseStack, font, title, width / 2, height / 2 - HEIGHT, 0xFFFFFF);

        // render the fields and their titles
        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.business_name").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 -25 - 12, 0xFFFFFF);
        nameField.render(poseStack, mouseX, mouseY, partialTicks);

        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.exchange_item").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +15 - 12, 0xFFFFFF);
        exchangeItemField.render(poseStack, mouseX, mouseY, partialTicks);

        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.exchange_currency").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +55 - 12, 0xFFFFFF);
        exchangeCurrencyField.render(poseStack, mouseX, mouseY, partialTicks);

        drawString(poseStack, font, new TranslatableComponent("menus.themoneymod.initial_stock_price").getString(), width / 2 - WIDTH / 2 + 10, height / 2 - HEIGHT / 2 +95 - 12, 0xFFFFFF);
        exchangeRatioField.render(poseStack, mouseX, mouseY, partialTicks);



        drawString(poseStack, font, piecesOfPaper() + "/64 "+new TranslatableComponent("menus.themoneymod.pieces_of_paper").getString(), width / 2 -WIDTH/2+10, height / 2 - HEIGHT / 2 +135 - 12, 0xFFFFFF);

        confirmButton.render(poseStack, mouseX, mouseY, partialTicks);

        super.render(poseStack, mouseX, mouseY, partialTicks);
    }

    // a method to handle name changes
    private void onNameChanged(String name) {
        // check if the name is valid
        nameValid = name != null && !name.isEmpty() && name.matches("[A-Za-z0-9 :]+");

        // enable or disable the button accordingly
        confirmButton.active = ratioValid && nameValid && itemValid && piecesOfPaper()>=64
                && hasSpaceForShares() && !BuisnessesSaveData.isNameTaken(name)
                && CurrenciesSaveData.isNameTaken(exchangeCurrencyField.getValue());
    }

    private void onExchangeItemChanged(String name) {
        // check if the item exists in the game
        if (name.matches("[A-Za-z0-9/._:-]+")) {
            try {
                Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
                itemValid = !item.toString().equals("air");
            } catch (Exception e) {
                itemValid = false;
            }
        } else {
            itemValid = false;
        }


        System.out.println(itemValid + " " + nameValid + " " + piecesOfPaper() + " " + hasSpaceForShares());
        // enable or disable the button accordingly
        /*confirmButton.active = ratioValid && nameValid && itemValid && piecesOfPaper()>=64
                && hasSpaceForShares() && !BuisnessesSaveData.isNameTaken(name)
                && CurrenciesSaveData.isNameTaken(exchangeCurrencyField.getValue());*/
    }

    private void onExchangeCurrencyChanged(String s) {
        confirmButton.active = ratioValid && nameValid && itemValid &&
                piecesOfPaper()>=64 && hasSpaceForShares() && !BuisnessesSaveData.isNameTaken(nameField.getValue())
                && CurrenciesSaveData.isNameTaken(s);
    }

    private void onExchangeRatioChanged(String name) {
        // check if the item exists in the game
        if (name.matches("[0-9]+")) {
            ratioValid = true;
        }

        confirmButton.active = ratioValid && nameValid && itemValid &&
                piecesOfPaper()>=64 && hasSpaceForShares() && !BuisnessesSaveData.isNameTaken(name)
                && CurrenciesSaveData.isNameTaken(exchangeCurrencyField.getValue());
    }

    // a method to handle confirm button press
    public void onConfirmPressed(Button button) {
        ModPackets.sendToServer(new MakeBusiness(nameField.getValue(), exchangeItemField.getValue(), exchangeRatioField.getValue(), exchangeCurrencyField.getValue()));

        // close the menu
        minecraft.setScreen(null);
    }


    private int piecesOfPaper(){

        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);

            if (!stack.isEmpty() && stack.getItem().toString().equals("paper")) {
                // Add the stack size to the counter
                count += stack.getCount();
            }
        }


        return count;
    }

    private Boolean hasSpaceForShares(){
        int count = 0;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++) {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack.isEmpty()) {
                count = count + 64;
            }
        }
        return count >= 256;
    }

}





