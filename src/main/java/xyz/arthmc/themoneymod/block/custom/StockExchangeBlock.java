package xyz.arthmc.themoneymod.block.custom;

import com.mojang.logging.LogUtils;
import io.netty.handler.codec.mqtt.MqttProperties;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.entity.ModBlockEntities;
import xyz.arthmc.themoneymod.block.entity.custom.StockExchangeBlockEntity;
import xyz.arthmc.themoneymod.screen.StockExchangeMenu;
import xyz.arthmc.themoneymod.screen.StockExchangeSetupMenu;
import xyz.arthmc.themoneymod.util.ScreenOpener;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import javax.annotation.Nullable;

public class StockExchangeBlock extends BaseEntityBlock {
    private static final Logger LOGGER = LogUtils.getLogger();
    public static final EnumProperty<Facing> FACING = EnumProperty.create("facing", Facing.class);

    public static final IntegerProperty[] BUSINESS = new IntegerProperty[8000];









    public StockExchangeBlock(Properties properties) {
        super(properties);



    }



    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        pBuilder.add(FACING);


    }

    public ExchangeName stringToEnum(String name) {
        for (ExchangeName exchangeName : ExchangeName.values()) {
            if (exchangeName.getSerializedName().equals(name)) {
                return exchangeName;
            }
        }
        return null;
    }

    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return (BlockState)this.defaultBlockState().setValue(FACING, Facing.fromDirection(pContext.getHorizontalDirection()));
    }


    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {


        // check if the player is on the client side
        if (pLevel.isClientSide) {

            //if the block entity.setup is false, open the setup menu
            /*if (!((StockExchangeBlockEntity) pLevel.getBlockEntity(pPos)).setup) {
                Minecraft.getInstance().setScreen(new StockExchangeSetupMenu(pPos));
            } else {*/
                //if the block entity.setup is true, open the stock exchange menu
            ScreenOpener.setBlockPos(pPos.getX()+","+pPos.getY()+","+pPos.getZ());
            ScreenOpener.open("StockExchangeMenu");
           // }
        }

        // log the interaction result
        LOGGER.info("Interaction result: " + InteractionResult.PASS);
        // return the interaction result
        return InteractionResult.PASS;
    }



    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }


    //implement MenuProvider
    @Nullable
    public MenuProvider getMenuProvider(BlockState pState, Level pLevel, BlockPos pPos) {
        BlockEntity blockEntity = pLevel.getBlockEntity(pPos);
        return blockEntity instanceof MenuProvider ? (MenuProvider)blockEntity : null;
    }


    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if (pState.getBlock() != pNewState.getBlock()) {
            BlockEntity blockEntity = pLevel.getBlockEntity(pPos);
            if (blockEntity instanceof StockExchangeBlockEntity) {
                ((StockExchangeBlockEntity) blockEntity).drops();
                StockExchangesSaveData.removeExchange(pPos.getX() + "," + pPos.getY() + "," + pPos.getZ());

            }

            super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
        }
    }
    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {

        return new StockExchangeBlockEntity(pPos, pState);
    }





    public enum Facing implements StringRepresentable {
        NORTH("north"),
        SOUTH("south"),
        EAST("east"),
        WEST("west");

        private final String name;

        Facing(String name) {
            this.name = name;
        }

        public static Facing fromDirection(Direction facing) {
            switch (facing) {
                case NORTH:
                    return NORTH;
                case SOUTH:
                    return SOUTH;
                case EAST:
                    return EAST;
                case WEST:
                    return WEST;
                default:
                    return NORTH;
            }
        }

        public String getSerializedName() {
            return name;
        }
    }

    public enum ExchangeName implements StringRepresentable {
        STOCK_EXCHANGE("Stock Exchange");

        private final String name;

        ExchangeName(String name) {
            this.name = name;
        }

        public String getSerializedName() {
            return name;
        }
    }
    public enum FillLevel implements StringRepresentable {
        EMPTY("empty"),
        LOW("low"),
        HIGH("high"),
        FULL("full");

        private final String name;

        FillLevel(String name) {
            this.name = name;
        }


        public String getSerializedName() {
            return name;
        }
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {

        return createTickerHelper(pBlockEntityType, (BlockEntityType<StockExchangeBlockEntity>) ModBlockEntities.STOCK_EXCHANGE_BLOCK_ENTITY.get(), StockExchangeBlockEntity::tick);
    }


}
