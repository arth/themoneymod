package xyz.arthmc.themoneymod.block.custom;

import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import org.slf4j.Logger;
import xyz.arthmc.themoneymod.block.entity.ModBlockEntities;
import xyz.arthmc.themoneymod.block.entity.custom.BasicBlockEntity;
import xyz.arthmc.themoneymod.screen.StockSetMenu;
import xyz.arthmc.themoneymod.util.ScreenOpener;

import javax.annotation.Nullable;

public class StockSetBlock extends BaseEntityBlock {
    private static final Logger LOGGER = LogUtils.getLogger();
    public static final EnumProperty<Facing> FACING = EnumProperty.create("facing", Facing.class);

    public StockSetBlock(Properties properties) {
        super(properties);
        //detect where the player is looking
        //set the facing property to that direction


    }



    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        pBuilder.add(FACING);
    }

    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return (BlockState)this.defaultBlockState().setValue(FACING, Facing.fromDirection(pContext.getHorizontalDirection().getOpposite()));
    }


    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {


        ScreenOpener.open("StockSetMenu");


        // log the interaction result
        LOGGER.info("Interaction result: " + InteractionResult.PASS);
        // return the interaction result
        return InteractionResult.PASS;
    }



    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }


    //implement MenuProvider
    @Nullable
    public MenuProvider getMenuProvider(BlockState pState, Level pLevel, BlockPos pPos) {
        BlockEntity blockEntity = pLevel.getBlockEntity(pPos);
        return blockEntity instanceof MenuProvider ? (MenuProvider)blockEntity : null;
    }


    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if (pState.getBlock() != pNewState.getBlock()) {
            BlockEntity blockEntity = pLevel.getBlockEntity(pPos);
            if (blockEntity instanceof BasicBlockEntity) {
                //((TapBlockEntity) blockEntity).drops();
            }

            super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
        }
    }
    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return new BasicBlockEntity(pPos, pState);
    }





    public enum Facing implements StringRepresentable {
        NORTH("north"),
        SOUTH("south"),
        EAST("east"),
        WEST("west");

        private final String name;

        Facing(String name) {
            this.name = name;
        }

        public static Facing fromDirection(Direction facing) {
            switch (facing) {
                case NORTH:
                    return NORTH;
                case SOUTH:
                    return SOUTH;
                case EAST:
                    return EAST;
                case WEST:
                    return WEST;
                default:
                    return NORTH;
            }
        }

        public String getSerializedName() {
            return name;
        }
    }
    public enum FillLevel implements StringRepresentable {
        EMPTY("empty"),
        LOW("low"),
        HIGH("high"),
        FULL("full");

        private final String name;

        FillLevel(String name) {
            this.name = name;
        }


        public String getSerializedName() {
            return name;
        }
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {

        return createTickerHelper(pBlockEntityType, (BlockEntityType<BasicBlockEntity>) ModBlockEntities.BASE_BLOCK_ENTITY.get(), BasicBlockEntity::tick);
    }


}
