package xyz.arthmc.themoneymod.block.entity.custom;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.Containers;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.registries.ForgeRegistries;
import xyz.arthmc.themoneymod.block.ModBlocks;
import xyz.arthmc.themoneymod.block.entity.ModBlockEntities;
import xyz.arthmc.themoneymod.item.ModItems;
import xyz.arthmc.themoneymod.util.StockExchangesSaveData;

import static java.lang.Integer.parseInt;

public class StockExchangeBlockEntity extends BlockEntity {

    private static String[][] businesses = new String[8000][8];
    public Boolean setup = false;

    public String exchangeName = "Stock Exchange";

    private String position;

    public StockExchangeBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        super(ModBlockEntities.STOCK_EXCHANGE_BLOCK_ENTITY.get(), pWorldPosition, pBlockState);
        for (int i = 0; i < 8000; i++) {
            businesses[i][0] = "Test";
            businesses[i][1] = "" + (int) (Math.random() * 100);
            businesses[i][2] = "minecraft:emerald";
            businesses[i][3] = "undefined";
            businesses[i][4] = "undefined";
            businesses[i][5] = "undefined";
            businesses[i][6] = "undefined";
            businesses[i][7] = "undefined";
        }

        position = pWorldPosition.getX() + "," + pWorldPosition.getY() + "," + pWorldPosition.getZ();


    }

    public static void tick(Level pLevel, BlockPos pBlockPos, BlockState pState, StockExchangeBlockEntity pBlockEntity) {

        //every 5 minutes...

        /*if (!pLevel.isClientSide) {
            if (pLevel.getGameTime() % 20 == 0) {
                System.out.println("tick");
                for(int i = 0; i < 8000; i++) {
                int rand = (int) (Math.random() * 100);
                if (rand < 40 ) {
                    //fluctuate stock price
                    int rand2 = (int) (Math.random() * 12);
                    if (rand2 < 6) {
                        rand2 *= -1;
                    } else {
                        rand2 -= 6;
                    }
                    rand2 /= 2;
                    businesses[i][1] = (Integer.parseInt(businesses[i][1]) + rand2) + "";
                }

                }
            }
        } else if (pLevel.getGameTime() % 20 == 0) {
            System.out.println("tick2");
        }*/




    }



    public String[][] getBusinesses() {
        return businesses;
    }




    public void drops() {

        SimpleContainer inventory = new SimpleContainer(1001);
        String[] exchange = StockExchangesSaveData.getExchange(position);
        if (exchange != null) {
            for (int i = 0; i < 1000; i++) {
                if (exchange[i] != null) {
                    if (exchange[i].contains("stock;")) {
                        String[] split = exchange[i].split(";");
                        ItemStack shareCertificate = new ItemStack(ModItems.SHARE_CERTIFICATE.get(), parseInt(split[2]));
                        shareCertificate.getOrCreateTag().putString("businessName", split[1]);
                        inventory.setItem(i, shareCertificate);
                    }
                    if (exchange[i].contains("item;")) {
                        String[] split = exchange[i].split(";");
                        Item item = ForgeRegistries.ITEMS.getValue((new ResourceLocation(split[3])));
                        ItemStack itemStack = new ItemStack(item, parseInt(split[2]));
                        inventory.setItem(i, itemStack);
                    }
                }
            }


            //gets block id, drop the block
            String blockId = this.getLevel().getBlockState(worldPosition).getBlock().getRegistryName().toString();
            System.out.println("12678 " + blockId);
            if (blockId.equals("themoneymod:stock_exchange")) {
                inventory.setItem(1000, new ItemStack(ModBlocks.STOCK_EXCHANGE.get()));
            } else if (blockId.equals("themoneymod:secured_stock_exchange")) {
                inventory.setItem(1000, new ItemStack(ModBlocks.SECURED_STOCK_EXCHANGE.get()));
            }

            //shrink the inventory to the size of the amount of items in it
            //for optimization reasons
            SimpleContainer inventory2 = new SimpleContainer(inventory.getContainerSize());
            for (int i = 0; i < inventory.getContainerSize(); i++) {
                if (inventory.getItem(i) != ItemStack.EMPTY) {
                    inventory2.setItem(i, inventory.getItem(i));
                }
            }
            Containers.dropContents(this.getLevel(), this.getBlockPos(), inventory);
        }






    }

    //a GUI with two text fields, one saying "What is your buisness name?" and the other saying "How many shares does this buisness have?"
//a button that says "Create Buisness"
    //if the user clicks the button, it writes to a world data file the name of the buisness and the amount of shares it has


   /* public Component getDisplayName() {
        return new TranslatableComponent("container.themoneymod.buisness_box");
    }*/

    /*@Nullable
    @Override
    public AbstractContainerMenu createMenu(int pId, Inventory pPlayerInventory, Player pPlayer) {
        return new BuisnessBoxMenu(pId, pPlayerInventory);
    }*/









    /*@Nullable

    public AbstractContainerMenu createMenu(BlockPos pContainerId, BlockState pInventory, Player pPlayer) {
        return new BuisnessBoxMenu(pContainerId, pInventory);
    }*/
}
