package xyz.arthmc.themoneymod.block.entity;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import xyz.arthmc.themoneymod.TheMoneyMod;
import xyz.arthmc.themoneymod.block.ModBlocks;
import xyz.arthmc.themoneymod.block.entity.custom.BasicBlockEntity;
import xyz.arthmc.themoneymod.block.entity.custom.StockExchangeBlockEntity;

public class ModBlockEntities {
    public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITIES =
            DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, TheMoneyMod.MOD_ID);

    public static final RegistryObject<BlockEntityType<BasicBlockEntity>> BASE_BLOCK_ENTITY = BLOCK_ENTITIES.register("base_block_entity",
            () -> BlockEntityType.Builder.of(BasicBlockEntity::new, ModBlocks.RADIO_STOCK_SET.get()).build(null));

    public static final RegistryObject<BlockEntityType<StockExchangeBlockEntity>> STOCK_EXCHANGE_BLOCK_ENTITY = BLOCK_ENTITIES.register("stock_exchange_block_entity",
            () -> BlockEntityType.Builder.of(StockExchangeBlockEntity::new, ModBlocks.STOCK_EXCHANGE.get()).build(null));


    public static void register(IEventBus eventBus) {
        BLOCK_ENTITIES.register(eventBus);
    }

}
