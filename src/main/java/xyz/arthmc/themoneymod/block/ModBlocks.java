package xyz.arthmc.themoneymod.block;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import xyz.arthmc.themoneymod.TheMoneyMod;
import xyz.arthmc.themoneymod.block.custom.CurrencyPrinterBlock;
import xyz.arthmc.themoneymod.block.custom.StockExchangeBlock;
import xyz.arthmc.themoneymod.block.custom.StockSetBlock;
import xyz.arthmc.themoneymod.item.ModItems;

import java.util.function.Supplier;

public class ModBlocks {
    public static final DeferredRegister<Block> BLOCKS =
            DeferredRegister.create(ForgeRegistries.BLOCKS, TheMoneyMod.MOD_ID);

    public static final RegistryObject<Block> RADIO_STOCK_SET = registerBlock("radio_stock_set", () -> new StockSetBlock(BlockBehaviour.Properties.of(Material.METAL).strength(1f).noOcclusion()), CreativeModeTab.TAB_TOOLS);
    public static final RegistryObject<Block> DIGITAL_STOCK_SET = registerBlock("digital_stock_set", () -> new StockSetBlock(BlockBehaviour.Properties.of(Material.METAL).strength(1f).noOcclusion()), CreativeModeTab.TAB_TOOLS);


    public static final RegistryObject<Block> STOCK_EXCHANGE = registerBlock("stock_exchange", () -> new StockExchangeBlock(BlockBehaviour.Properties.of(Material.METAL).strength(12f).noOcclusion()), CreativeModeTab.TAB_TOOLS);

    public static final RegistryObject<Block> SECURED_STOCK_EXCHANGE = registerBlock("secured_stock_exchange", () -> new StockExchangeBlock(BlockBehaviour.Properties.of(Material.METAL).strength(210f).noOcclusion()), CreativeModeTab.TAB_TOOLS);

    public static final RegistryObject<Block> CURRENCY_PRINTER = registerBlock("currency_printer", () -> new CurrencyPrinterBlock(BlockBehaviour.Properties.of(Material.METAL).strength(1f).noOcclusion()), CreativeModeTab.TAB_TOOLS);





    private static <T extends Block>RegistryObject<T> registerBlock(String name, Supplier<T> block, CreativeModeTab tab) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn, tab);
        return toReturn;
    }
    private static <T extends Block>RegistryObject<Item> registerBlockItem(String name, Supplier<T> block, CreativeModeTab tab) {
        return ModItems.ITEMS.register(name, () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
    }
    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }
}
